using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResearchList
{
    UpdateMine,
    UpdateFireRate,
    ReduceBombCost
}
