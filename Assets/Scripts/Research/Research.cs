using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Research : MonoBehaviour
{
    [SerializeField] private ResearchList researchType;
    [SerializeField] private string researchText;
    [SerializeField] private Sprite icon = null;
    [SerializeField] private int price = 1;

    private RTSPlayer player;

    public string GetText()
    {
        return researchText;
    }
    public int GetPrice()
    {
        return price;
    }
    public Sprite GetIcon()
    {
        return icon;
    }

    private void Start()
    {
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();
    }

    public void MakeResearch()
    {
        switch(researchType)
        {
            case ResearchList.UpdateMine:
                UpdateMine();
                break;
            case ResearchList.UpdateFireRate:
                UpdateFireRate();
                break;
            case ResearchList.ReduceBombCost:
                ReduceBombCost();
                break;
            default:
                Debug.LogError("Wrong enum called");
                break;
        }
    }

    #region Research

    private void UpdateMine()
    {
        player.CmdSetUpdateResources(5);
    }
    private void UpdateFireRate()
    {
        player.CmdSetUpdateFireRate(10);
    }
    private void ReduceBombCost()
    {
        player.CmdSetReduceBombCost(50);
    }

    #endregion
}
