using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResearchButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Research research = null;
    [SerializeField] private Image iconImage = null;
    [SerializeField] private TMP_Text priceText = null;
    [SerializeField] private GameObject descriptionParent = null;
    [SerializeField] private TMP_Text descriptionText = null;
    [SerializeField] private Button button = null;

    private RTSPlayer player;

    private void Start()
    {
        descriptionParent.SetActive(false);
        button.interactable = true;

        priceText.text = research.GetPrice().ToString();
        iconImage.sprite = research.GetIcon();

        descriptionText.text = research.GetText();

        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //check
        if (player.GetResearchPoints() < research.GetPrice()) return;

        player.CmdSetResearchPoints(player.GetResearchPoints() - research.GetPrice());

        //make
        research.MakeResearch();

        //end
        button.interactable = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        descriptionParent.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        descriptionParent.SetActive(false);
    }
}
