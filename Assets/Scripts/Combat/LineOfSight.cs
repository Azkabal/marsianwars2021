using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LineOfSight : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private SphereCollider myCollider = null;
    [SerializeField] private Targetable myTargetable = null;

    [Header("Settings")]
    [SerializeField]
    private LayerMask lineOfSightMask = new LayerMask();
    [SerializeField] private float sightRange = 10f;
    [SerializeField] private bool checkOnAliens = true;
    [SerializeField] private bool checkOnConnection = true;


    List<Targetable> listOfPossibleTargets = new List<Targetable>();



    public Targetable GetTargetInSight()
    {
        Targetable returnTarget = null;

        if(listOfPossibleTargets.Count != 0)
        {
            if(listOfPossibleTargets[0] == null)
            {
                listOfPossibleTargets.Remove(listOfPossibleTargets[0]);
            }
            else
            {
                returnTarget = listOfPossibleTargets[0];
            }
        }

        return returnTarget;
    }

    public float GetSightRange()
    {
        return sightRange;
    }

    private void Start()
    {
        myCollider.radius = sightRange;

        Collider[] colliders = Physics.OverlapSphere(transform.position,
                              sightRange,
                              lineOfSightMask);

        if (colliders.Length > 0)
        {
            foreach (Collider col in colliders)
            {
                if (col == null) continue;

                if (col.TryGetComponent<Targetable>(out Targetable possibleTarget))
                {
                    if(CheckTarget(possibleTarget))
                    {
                        listOfPossibleTargets.Add(possibleTarget);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == null) return;

        if (!other.TryGetComponent<Targetable>(out Targetable target)) return;

        if(CheckTarget(target))
        {
            listOfPossibleTargets.Add(target);
        }
    }

    private bool CheckTarget(Targetable target)
    {
        bool returnValue = false;

        if (myTargetable == target) return false;

        if (target.tag == "Alien")
        {
            if (checkOnAliens) returnValue = true;
        }
        else
        {
            if(!target.connectionToClient.identity.hasAuthority)
            {
                if (checkOnConnection) returnValue = true;
            }

            if (!checkOnConnection && !checkOnAliens) returnValue = true; //alien server object case
        }

        return returnValue;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == this) return;

        if (!other.TryGetComponent<Targetable>(out Targetable target)) return;

        listOfPossibleTargets.Remove(target);
    }
}
