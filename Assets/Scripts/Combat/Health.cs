using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : NetworkBehaviour
{
    [SerializeField] private GameObject VFXprefab = null;
    [SerializeField] private int maxHealth = 100;
    [SerializeField] private bool isBuilding = false;

    [SyncVar(hook = nameof(HandleHealthUpdated))]
    private int currentHealth;

    public event Action ServerOnDie;

    public event Action<int, int> ClientOnHealthUpdated;

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    #region Server

    public override void OnStartServer()
    {
        if(isBuilding)
        {
            currentHealth = maxHealth * 10 / 100;
        }
        else
        {
            currentHealth = maxHealth;
        }

        UnitBase.ServerOnPlayerDie += ServerHandlePlayerDie;
    }

    public override void OnStopServer()
    {
        UnitBase.ServerOnPlayerDie -= ServerHandlePlayerDie;
    }

    [Server]
    public void DealDamage(int damageAmount)
    {
        if(currentHealth == 0) { return; }

        //nice thing
        currentHealth = Mathf.Max(currentHealth - damageAmount, 0);

        if (currentHealth != 0) { return; }

        ServerOnDie?.Invoke();

        if (VFXprefab == null) return;

        GameObject newObject = Instantiate(VFXprefab, this.transform.position, Quaternion.identity);

        NetworkServer.Spawn(newObject);
    }

    [Server]
    public void AddHealth(int healthAmount)
    {
        currentHealth = Mathf.Min(currentHealth + healthAmount, maxHealth);
    }

    [Server]
    private void ServerHandlePlayerDie(int connectionId)
    {
        if(connectionId != connectionToClient.connectionId) { return; }

        DealDamage(currentHealth);
    }

    #endregion

    #region Client

    private void HandleHealthUpdated(int oldHealth, int newHealth)
    {
        ClientOnHealthUpdated?.Invoke(newHealth, maxHealth);
    }

    #endregion
}
