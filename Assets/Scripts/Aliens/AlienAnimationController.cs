using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AlienAnimationController : NetworkBehaviour
{
    [Header("References")]
    [SerializeField] private Health health = null;
    [SerializeField] private NavMeshAgent agent = null;
    [SerializeField] private UnitMeleeCombat unitMeleeCombat = null;
    [SerializeField] private AlienMovementController controller = null;
    // alien movement
    [Range(1,4)]
    [SerializeField] private int numberOfAttackAnimation = 4;
    [SerializeField] private NetworkAnimator netAnimator = null;

    private bool
        isDead = false;


    [ServerCallback]
    private void Update()
    {
        if(isDead)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y-0.2f*Time.deltaTime, transform.position.z);
        }

        netAnimator.animator.SetFloat("VelX", agent.velocity.x);
        netAnimator.animator.SetFloat("VelZ", agent.velocity.z);
    }
    private void DestroyMe()
    {
        NetworkServer.Destroy(gameObject);
    }


    #region Server
    public override void OnStartServer()
    {
        controller.ServerAnimateStateIntoIdle += ServerExtraAnimationIdle;
        controller.ServerAnimateStateIntoAttack += ServerExtraAnimationAttack;
        controller.ServerAnimateStateIntoWaitForTarget += ServerExtraAnimationWaitForTarget;
        controller.ServerAnimateStateIntoReturnHome += ServerExtraAnimationReturnHome;
        unitMeleeCombat.AnimationAttack += AttackAnimation;
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        controller.ServerAnimateStateIntoIdle -= ServerExtraAnimationIdle;
        controller.ServerAnimateStateIntoAttack -= ServerExtraAnimationAttack;
        controller.ServerAnimateStateIntoWaitForTarget -= ServerExtraAnimationWaitForTarget;
        controller.ServerAnimateStateIntoReturnHome -= ServerExtraAnimationReturnHome;
        unitMeleeCombat.AnimationAttack -= AttackAnimation;
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void AttackAnimation()
    {
        int RandAnimation = (int) Random.Range((int)0, (int)numberOfAttackAnimation);

        switch (RandAnimation)
        {
            case 0:
                netAnimator.SetTrigger("Attack0");
                break;
            case 1:
                netAnimator.SetTrigger("Attack1");
                break;
            case 2:
                netAnimator.SetTrigger("Attack2");
                break;
            case 3:
                netAnimator.SetTrigger("Attack3");
                break;
        }
    }

    private void ServerExtraAnimationIdle()
    {
        netAnimator.SetTrigger("TakeDamage");
    }
    private void ServerExtraAnimationAttack()
    {

    }
    private void ServerExtraAnimationWaitForTarget()
    {

    }
    private void ServerExtraAnimationReturnHome()
    {
        netAnimator.SetTrigger("Roar");
    }

    private void ServerHandleDie()
    {
        netAnimator.SetTrigger("Die");
        isDead = true;
        Invoke("DestroyMe", 10f);
    }
    #endregion
}
