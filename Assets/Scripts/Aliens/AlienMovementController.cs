using Mirror;
using System;
using UnityEngine;
using UnityEngine.AI;

public class AlienMovementController : NetworkBehaviour
{
    public enum AlienStates
    {
        Idle,
        Attack,
        WaitForTarget,
        ReturnHome
    }

    public AlienStates currentState = AlienStates.ReturnHome;

    [Header("References")]
    [SerializeField] private Health health = null;
    [SerializeField] private NavMeshAgent agent = null;
    [SerializeField] private Targeter targeter = null;
    [SerializeField] private LineOfSight lineOfSight = null;
    [SerializeField] private Transform alienPosition = null;

    [Header("Settings")]
    [SerializeField] private float lookForEnemiesThreshold = 2f;
    [SerializeField] private float waitForEnemiesThreshold = 2f;
    [SerializeField] private float idleMoveThreshold = 5f; 
    [SerializeField] private float idleMoveRange = 5f;

    private float lastSeenTargetTime = 0f;
    private float waitForTargetTime = 0f;
    private float idleMoveTime = 0f;

    public event Action ServerAnimateStateIntoIdle;
    public event Action ServerAnimateStateIntoAttack;
    public event Action ServerAnimateStateIntoWaitForTarget;
    public event Action ServerAnimateStateIntoReturnHome;

    private Vector3 targetPosition;

    [ServerCallback]
    private void Update()
    {
        switch(currentState)
        {
            case AlienStates.Idle:
                StateIdle();
                break;
            case AlienStates.Attack:
                StateAttack();
                break;
            case AlienStates.WaitForTarget:
                StateWaitForTarget();
                break;
            case AlienStates.ReturnHome:
                StateReturnHome();
                break;
        }
    }
    private void StateIdle()
    {
        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = AlienStates.Attack;
            ServerAnimateStateIntoAttack?.Invoke();
            return;
        }

        LookForTarget();

        if (alienPosition == null) return;

        idleMoveTime += Time.deltaTime;
        if(idleMoveTime > idleMoveThreshold)
        {
            float randXpos = UnityEngine.Random.Range(-idleMoveRange, idleMoveRange);
            float randZpos = UnityEngine.Random.Range(-idleMoveRange, idleMoveRange);

            targetPosition = new Vector3(
                alienPosition.position.x + randXpos,
                transform.position.y,
                alienPosition.position.z + randZpos
                );

            if(agent.SetDestination(targetPosition))
            {
                idleMoveTime = Mathf.Epsilon;
            }
        }
        else
        {
            if ((targetPosition - transform.position).sqrMagnitude < 2f * 2f)
            {
                agent.ResetPath();
            }
        }
    }
    private void StateAttack()
    {
        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            waitForTargetTime = 0f;
            if ((target.transform.position - transform.position).sqrMagnitude > (lineOfSight.GetSightRange() * lineOfSight.GetSightRange()))
            {
                lastSeenTargetTime += Time.deltaTime;
            }
            else
            {
                lastSeenTargetTime = Mathf.Epsilon;
            }

            if (lastSeenTargetTime > lookForEnemiesThreshold)
            {
                targeter.ServerClearTarget();
                lastSeenTargetTime = Mathf.Epsilon;
                agent.ResetPath();
                currentState = AlienStates.WaitForTarget;
                ServerAnimateStateIntoWaitForTarget?.Invoke();
                return;
            }

            if ((target.transform.position - transform.position).sqrMagnitude >= ((agent.stoppingDistance * agent.stoppingDistance)))
            {
                agent.SetDestination(target.transform.position);
            }

            return;
        }
        else
        {
            currentState = AlienStates.WaitForTarget;
            ServerAnimateStateIntoWaitForTarget?.Invoke();
        }
    }
    private void StateWaitForTarget()
    {
        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = AlienStates.Attack;
            ServerAnimateStateIntoAttack?.Invoke();
            waitForTargetTime = 0;
            return;
        }

        LookForTarget();

        waitForTargetTime += Time.deltaTime;
        if (waitForTargetTime > waitForEnemiesThreshold)
        {
            waitForTargetTime = 0;
            currentState = AlienStates.ReturnHome;
            ServerAnimateStateIntoReturnHome?.Invoke();
            return;
        }
    }
    private void StateReturnHome()
    {
        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = AlienStates.Attack;
            ServerAnimateStateIntoAttack?.Invoke();
            waitForTargetTime = 0;
            return;
        }

        LookForTarget();

        if ((alienPosition.transform.position - transform.position).sqrMagnitude >= ((2f * 2f)))
        {
            agent.SetDestination(alienPosition.transform.position);
        }
        else
        {
            agent.ResetPath();
            currentState = AlienStates.Idle;
            ServerAnimateStateIntoIdle?.Invoke();
            return;
        }
    }

    #region Server
    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void LookForTarget()
    {
        Targetable possibleTarget = lineOfSight.GetTargetInSight();

        if (possibleTarget != null)
        {
            ServerTrySetTarget(possibleTarget.gameObject);
            return;
        }

        return;
    }
    [Server]
    private void ServerTrySetTarget(GameObject possibleTarget)
    {
        if (possibleTarget == null) return;
        if (possibleTarget.GetComponent<Targetable>() == null) return;
        //if(possibleTarget == gameObject) return;

        targeter.ServerSetTarget(possibleTarget);
    }

    [Server]
    private void ServerHandleDie()
    {
        agent.enabled = false;

        GetComponent<SphereCollider>().enabled = false;
        GetComponent<Targetable>().enabled = false;
        Destroy(GetComponent<Targetable>());
        enabled = false;
    }
    #endregion
}
