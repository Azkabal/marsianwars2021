using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GraphicSettingWorker : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown dropdown = null;

    const string PlayerPrefsGraphicLevelKey = "GraphicLevel";

    private void Awake()
    {

        if (!PlayerPrefs.HasKey(PlayerPrefsGraphicLevelKey))
        {
            int unityGraphicLevel = QualitySettings.GetQualityLevel();

            dropdown.SetValueWithoutNotify(unityGraphicLevel);

            return;
        }

        int defaultGraphicLevel = PlayerPrefs.GetInt(PlayerPrefsGraphicLevelKey);

        dropdown.SetValueWithoutNotify(defaultGraphicLevel);
    }

    // Call per button
    public void SetGraphicSettingFloat(float newSetting)
    {
        QualitySettings.SetQualityLevel((int)newSetting);

        PlayerPrefs.SetInt(PlayerPrefsGraphicLevelKey, (int)newSetting);
    }
    // Call per button
    public void SetGraphicSettingInt(System.Int32 newSetting)
    {
        QualitySettings.SetQualityLevel((int)newSetting);

        PlayerPrefs.SetInt(PlayerPrefsGraphicLevelKey, (int)newSetting);
    }
}
