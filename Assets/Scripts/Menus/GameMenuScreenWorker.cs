using Mirror;
using UnityEngine;

public class GameMenuScreenWorker : MonoBehaviour
{
    [Header("Menu panels")]
    [SerializeField] private GameObject        mainMenuPanel = null;
    [SerializeField] private GameObject        campaignPanel = null;
    [SerializeField] private GameObject        multiplayerPanel = null;
    [SerializeField] private GameObject        settingsPanel = null;
    [SerializeField] private RTSNetworkManager rtsNetworkManager = null;

    #region unity callbacks

    private void Awake()
    {
        if (mainMenuPanel == null) { Debug.LogError("MainMenuPanel not connected"); return; }
        if (campaignPanel == null) { Debug.LogError("CampaignPanel not connected"); return; }
        if (multiplayerPanel == null) { Debug.LogError("MultiplayerPanel not connected"); return; }
        if (settingsPanel == null) { Debug.LogError("SettingsPanel not connected"); return; }
        if (rtsNetworkManager == null) { Debug.LogError("rtsNetworkManager not connected"); return; }

        mainMenuPanel.SetActive(true);
        campaignPanel.SetActive(false);
        multiplayerPanel.SetActive(false);
        settingsPanel.SetActive(false);
    }

    #endregion

    #region button

    public void StartNewCampaign()
    {
        HostLobby();

        Invoke("StartGame", 0.2f);
    }

    private void HostLobby()
    {
        NetworkManager.singleton.StartHost();

    }

    private void StartGame()
    {
        NetworkClient.connection.identity.GetComponent<RTSPlayer>().CmdStartGame();
    }

    //called per Button
    public void ExitApplicationOnButton()
    {
        Application.Quit();
    }

    //called per Button
    public void BackToTop()
    {
        mainMenuPanel.SetActive(true);
        campaignPanel.SetActive(false);
        multiplayerPanel.SetActive(false);
        settingsPanel.SetActive(false);
    }

    #endregion
}
