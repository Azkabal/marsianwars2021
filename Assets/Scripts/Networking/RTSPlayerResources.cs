using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSPlayerResources : NetworkBehaviour
{
    [SyncVar(hook = nameof(ClientHandleResourcesUpdated))]
    private int resources = 500;

    [SyncVar]
    private int resourcesMaxCapacity = 2000;

    [SyncVar]
    private int currentEnergy = 0;

    private int energyLimit = 100;


    public event Action<int, int> ClientOnResourcesUpdated;

    #region getResources
    public int GetResources()
    {
        return resources;
    }
    public int GetResourcesMaxCapacity()
    {
        return resourcesMaxCapacity;
    }

    public int GetCurrentEnergy()
    {
        return currentEnergy;
    }

    public int GetEnergyLimit()
    {
        return energyLimit;
    }
    #endregion

    #region setResources
    [Command]
    public void CmdSetResources(int newResources)
    {
        if (newResources >= resourcesMaxCapacity)
        {
            resources = resourcesMaxCapacity;
        }
        else
        {
            resources = newResources;
        }
    }

    [Server]
    public void ServerSetResources(int newResources)
    {
        if (newResources >= resourcesMaxCapacity)
        {
            resources = resourcesMaxCapacity;
        }
        else
        {
            resources = newResources;
        }
    }

    [Server]
    public void SetResourcesMaxCapacity(int newResourcesMaxCapacity)
    {
        resourcesMaxCapacity = newResourcesMaxCapacity;

        ClientOnResourcesUpdated?.Invoke(resources, newResourcesMaxCapacity);
    }

    [Server]
    public void ServerSetEnergy(int newEnergy)
    {
        currentEnergy = Mathf.Clamp(newEnergy, -10000, energyLimit);
    }
    [Command]
    public void CmdSetEnergy(int newEnergy)
    {
        currentEnergy = Mathf.Clamp(newEnergy, -10000, energyLimit);
    }
    #endregion

    private void ClientHandleResourcesUpdated(int oldResources, int newResources)
    {
        ClientOnResourcesUpdated?.Invoke(newResources, resourcesMaxCapacity);
    }
}
