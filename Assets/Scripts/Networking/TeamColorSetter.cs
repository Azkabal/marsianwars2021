using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamColorSetter : NetworkBehaviour
{
    [SyncVar(hook = nameof(ClientHandleTeamColorUpdate))]
    private Color teamColor = new Color();

    RTSPlayer player = null;

    #region Server

    public override void OnStartServer()
    {
        player = connectionToClient.identity.GetComponent<RTSPlayer>();

        teamColor = player.GetTeamColor();
    }

    #endregion

    #region Client

    private void ClientHandleTeamColorUpdate(Color oldColor, Color newColor)
    {
        if (!hasAuthority) return;

        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

        foreach (Renderer renderer in renderers)
        {
            List<Material> materials = new List<Material>();
            renderer.GetMaterials(materials);

            foreach (Material material in materials)
            {
                if(material.name == "TeamColorSetter (Instance)")
                {
                    material.SetColor("_BaseColor", newColor);
                }
            }
        }
    }

    #endregion
}
