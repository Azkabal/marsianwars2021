using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RTSPlayer : NetworkBehaviour
{
    [Header("References")]
    [SerializeField] public RTSPlayerResources playerResources = null;
    [SerializeField] private Transform cameraTransform = null;


    [Header("Settings")]
    [SerializeField] private LayerMask buildingBlockLayer = new LayerMask();
    [SerializeField] private LayerMask generatorBlockLayer = new LayerMask();
    [SerializeField] private Building[] buildings = new Building[0];
    [SerializeField] private float buildingRangeLimit = 5f;

    #region Research
    [SyncVar(hook = nameof(ClientHandleResearchPointsUpdated))]
    private int researchPoints = 0;

    [SyncVar(hook = nameof(ServerHandleUpdatedResources))] private int resourceUpdated = 0;
    public event Action<int> ServerOnResourcesFlowChanged;

    [SyncVar] private int fireRateUpdated = 0;

    [SyncVar(hook = nameof(ClientHandleReduceBombCost))] private int reduceBombCost = 0;

    [ServerCallback]
    private void ServerHandleUpdatedResources(int oldResources, int newResources)
    {
        ServerOnResourcesFlowChanged?.Invoke(newResources);
    }
    public int GetUpdateFireRate()
    {
        return fireRateUpdated;
    }
    public int GetUpdateResources()
    {
        return resourceUpdated;
    }
    public int GetReduceBombCost()
    {
        return reduceBombCost;
    }
    [Command]
    public void CmdSetUpdateResources(int newValue)
    {
        SetUpdateResources(newValue);
    }
    [Server]
    public void SetUpdateResources(int newValue)
    {
        resourceUpdated = newValue;
    }
    [Command]
    public void CmdSetUpdateFireRate(int newValue)
    {
        SetUpdateFireRate(newValue);
    }
    [Server]
    public void SetUpdateFireRate(int newValue)
    {
        fireRateUpdated = newValue;
    }

    [Command]
    public void CmdSetReduceBombCost(int newValue)
    {

        SetReduceBombCost(newValue);
    }

    [Server]
    public void SetReduceBombCost(int newValue)
    {
        reduceBombCost = newValue;
    }

    [ClientCallback]
    private void ClientHandleReduceBombCost(int oldValue, int newValue)
    {
        ClientOnBombCostUpdatedUpdated?.Invoke();
    }
    #endregion

    [SyncVar(hook = nameof(AuthorityHandlePartyOwnerStateUpdated))]
    private bool isPartyOwner = false;

    [SyncVar(hook = nameof(ClientHandleDisplayNameUpdated))]
    private string displayName = "";


    //events that are called GLOBAL!
    public static event Action ClientOnInfoUpdated;
    public static event Action<bool> AuthorityOnPartyOwnerStateUpdated;

    //events that are called locally!
    public event Action ClientOnBombCostUpdatedUpdated;
    public event Action<int> ClientOnResearchUpdated;

    private Color teamColor = new Color();
    private List<Unit> myUnits = new List<Unit>();
    private List<Building> myBuildings = new List<Building>();

    public int GetResearchPoints()
    {
        return researchPoints;
    }

    public string GetDisplayName()
    {
        return displayName;
    }

    public bool GetIsPartyOwner()
    {
        return isPartyOwner;
    }

    public Transform GetCameraTransform()
    {
        return cameraTransform;
    }
    public Color GetTeamColor()
    {
        return teamColor;
    }
    public List<Unit> GetMyUnits()
    {
        return myUnits;
    }
    public List<Building> GetMyBuildings()
    {
        return myBuildings;
    }

    public bool CanPlaceBuilding(BoxCollider buildingCollider, Vector3 point, float rotationYaxis)
    {
        if (buildingCollider == null) return false;

        if (Physics.CheckBox(
            point + buildingCollider.center,
            buildingCollider.size / 2,
            Quaternion.Euler(0, rotationYaxis, 0),
            buildingBlockLayer))
        {
            return false;
        }

        foreach (Building building in myBuildings)
        {
            if (!building.IsFinished()) continue;

            /*the influence range can be only within radar towers, unit base*/
            if ((!building.GetComponent<PlaceholderBuilding>()) &&
                (!building.GetComponent<UnitBase>())) continue;

            //if ((building.GetComponent<BombDetonator>())) continue; //ignore bombs

            if ((point - building.transform.position).sqrMagnitude
                <= buildingRangeLimit * buildingRangeLimit)
            {
                return true;
            }
        }

        return false;
    }
    public bool CanPlaceGenerator(BoxCollider buildingCollider, Vector3 point, float rotationYaxis)
    {
        if (buildingCollider == null) return false;

        if (!Physics.CheckBox(
            point + buildingCollider.center,
            buildingCollider.size / 2,
            Quaternion.Euler(0, rotationYaxis, 0),
            generatorBlockLayer))
        {
            return false;
        }

        if (Physics.CheckBox(
            point + buildingCollider.center,
            buildingCollider.size / 2,
            Quaternion.Euler(0, rotationYaxis, 0),
            buildingBlockLayer))
        {
            return false;
        }

        foreach (Building building in myBuildings)
        {
            if (!building.IsFinished()) continue;

            /*the influence range can be only within radar towers, unit base*/
            if ((!building.GetComponent<PlaceholderBuilding>()) &&
            (!building.GetComponent<UnitBase>())) continue;


            if ((point - building.transform.position).sqrMagnitude
                <= buildingRangeLimit * buildingRangeLimit)
            {
                return true;
            }
        }

        return false;
    }

    [Server]
    public void SetDisplayName(string displayName)
    {
        this.displayName = displayName;
    }
    [Command]
    public void CmdSetDisplayName(string displayName)
    {
        SetDisplayName(displayName);
    }

    [Server]
    public void SetIsPartyOwner(bool state)
    {
        isPartyOwner = state;
    }

    [Command]
    public void CmdStartGame()
    {
        if(!isPartyOwner) { return; }

        ((RTSNetworkManager)NetworkManager.singleton).StartGame();
    }

    #region Server
    [Server]
    public void SetTeamColor(Color newTeamColor)
    {
        teamColor = newTeamColor;
    }




    [Command]
    public void CmdSetResearchPoints(int newResearch)
    {
        SetResearchPoints(newResearch);
    }
    [Server]
    public void SetResearchPoints(int newResearch)
    {
        researchPoints = newResearch;
    }

    public override void OnStartServer() 
    {
        Unit.ServerOnUnitSpawned += ServerHandleUnitSpawned;
        Unit.ServerOnUnitDespawned += ServerHandleUnitDespawned;
        Building.ServerOnBuildingSpawned += ServerHandleBuildingSpawned;
        Building.ServerOnBuildingDespawned += ServerHandleBuildingDespawned;

        if(playerResources == null)
        {
            Debug.LogError("Reference not set!");
        }

        DontDestroyOnLoad(gameObject); //singelton
    }

    public override void OnStopServer() //WHEN THIS STOP EXISTING ON THE SERVER
    {
        Unit.ServerOnUnitSpawned -= ServerHandleUnitSpawned;
        Unit.ServerOnUnitDespawned -= ServerHandleUnitDespawned;
        Building.ServerOnBuildingSpawned -= ServerHandleBuildingSpawned;
        Building.ServerOnBuildingDespawned -= ServerHandleBuildingDespawned;
    }

    [Command]
    public void CmdTryPlaceBuilding(int buildingId, Vector3 position, float rotationYaxis)
    {
        Building buildingToPlace = null;

        foreach(Building building in buildings)
        {
            if(building.GetId() == buildingId)
            {
                buildingToPlace = building;
                break;
            }
        }

        if(buildingToPlace == null) { return; }

        if (buildingId == 5) //todo : make this cleaner!
        {
            if (playerResources.GetResources() < (buildingToPlace.GetPrice() - reduceBombCost)) return;
        }
        else
        {
            if (playerResources.GetResources() < buildingToPlace.GetPrice()) return;
        }

        BoxCollider buildingCollider = buildingToPlace.GetComponent<BoxCollider>();

        bool generator = buildingId == 2;
        if(generator)
        {
            if (!CanPlaceGenerator(buildingCollider, position, rotationYaxis)) return;
        }
        else
        {
            if (!CanPlaceBuilding(buildingCollider, position, rotationYaxis)) return;
        }

        GameObject newBuilding = 
            Instantiate(buildingToPlace.gameObject, position, buildingToPlace.transform.rotation);

        newBuilding.transform.rotation = Quaternion.Euler(0, rotationYaxis, 0);

        NetworkServer.Spawn(newBuilding, connectionToClient);

        if (buildingId == 5) //TODO : make this cleaner!
        {
            playerResources.ServerSetResources(playerResources.GetResources() - (buildingToPlace.GetPrice() - reduceBombCost));
        }
        else
        {
            playerResources.ServerSetResources(playerResources.GetResources() - buildingToPlace.GetPrice());
        }
    }

    [Server]
    private void ServerHandleUnitSpawned(Unit unit)
    {
        if (unit == null) return;

        //important to check connection id because otherwise the wrong units can come to this list!
        if (unit.connectionToClient.connectionId != connectionToClient.connectionId)
            return;

        if(!myUnits.Contains(unit))
        {
            myUnits.Add(unit);
        }
    }
    [Server]
    private void ServerHandleUnitDespawned(Unit unit)
    {
        if (unit == null) return;

        //important to check connection id because otherwise the wrong units can come to this list!
        if (unit.connectionToClient.connectionId != connectionToClient.connectionId)
            return;

        if (myUnits.Contains(unit))
        {
            playerResources.ServerSetEnergy(playerResources.GetCurrentEnergy() + unit.GetEnergyCost());
            myUnits.Remove(unit);
        }
    }
    [Server]
    private void ServerHandleBuildingSpawned(Building building)
    {
        if (building == null) return;

        //important to check connection id because otherwise the wrong units can come to this list!
        if (building.connectionToClient.connectionId != connectionToClient.connectionId)
            return;

        if (!myBuildings.Contains(building))
        {
            myBuildings.Add(building);
        }
    }
    [Server]
    private void ServerHandleBuildingDespawned(Building building)
    {
        if (building == null) return;

        //important to check connection id because otherwise the wrong units can come to this list!
        if (building.connectionToClient.connectionId != connectionToClient.connectionId)
            return;

        if (myBuildings.Contains(building))
        {
            myBuildings.Remove(building);
        }
    }
    #endregion


    #region Client

    [ClientCallback]
    private void Start()
    {
        if(!hasAuthority) { return; }

        CmdSetDisplayName(PlayerPrefs.GetString("PlayerName"));
    }

    public override void OnStartAuthority()
    {
        if (NetworkServer.active) { return; } //preventing host to have duplicated lists

        Unit.AuthorityOnUnitSpawned += AuthorityHandleUnitSpawned;
        Unit.AuthorityOnUnitDespawned += AuthorityHandleUnitDespawned;
        Building.AuthorityOnBuildingSpawned += AuthorityHandleBuildingSpawned;
        Building.AuthorityOnBuildingDespawned += AuthorityHandleBuildingDespawned;
    }

    public override void OnStartClient()
    {
        if(NetworkServer.active) { return; }

        ((RTSNetworkManager)NetworkManager.singleton).players.Add(this);

        DontDestroyOnLoad(gameObject);
    }

    public override void OnStopClient()
    {
        ClientOnInfoUpdated?.Invoke();

        if (!isClientOnly) { return; } //preventing host to have duplicated lists

        ((RTSNetworkManager)NetworkManager.singleton).players.Remove(this);

        if(!hasAuthority) { return; }

        Unit.AuthorityOnUnitSpawned -= AuthorityHandleUnitSpawned;
        Unit.AuthorityOnUnitDespawned -= AuthorityHandleUnitDespawned;
        Building.AuthorityOnBuildingSpawned -= AuthorityHandleBuildingSpawned;
        Building.AuthorityOnBuildingDespawned -= AuthorityHandleBuildingDespawned;
    }

    private void ClientHandleDisplayNameUpdated(string oldName, string newName)
    {
        ClientOnInfoUpdated?.Invoke();
    }

    private void AuthorityHandlePartyOwnerStateUpdated(
        bool oldState, 
        bool newState)
    {
        if(!hasAuthority) { return; }

        AuthorityOnPartyOwnerStateUpdated?.Invoke(newState);
    }

    private void AuthorityHandleUnitSpawned(Unit unit)
    {
        if (unit == null) return;

        if (!myUnits.Contains(unit))
        {
            myUnits.Add(unit);
        }


    }

    private void AuthorityHandleUnitDespawned(Unit unit)
    {
        if (unit == null) return;

        if (myUnits.Contains(unit))
        {
            myUnits.Remove(unit);
        }
    }
    private void AuthorityHandleBuildingSpawned(Building building)
    {
        if (building == null) return;

        if (!myBuildings.Contains(building))
        {
            myBuildings.Add(building);
        }
    }

    private void AuthorityHandleBuildingDespawned(Building building)
    {
        if (building == null) return;

        if (myBuildings.Contains(building))
        {
            myBuildings.Remove(building);
        }
    }

    private void ClientHandleResearchPointsUpdated(int oldResearch, int newResearch)
    {
        ClientOnResearchUpdated?.Invoke(newResearch);
    }

    #endregion
}
