using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamColorMinimap : NetworkBehaviour
{
    [SerializeField] MeshRenderer minimap_renderer = null;

    [SyncVar(hook = nameof(ClientHandleTeamColorUpdate))]
    private Color teamColor = new Color();

    RTSPlayer player = null;

    #region Server

    public override void OnStartServer()
    {
        player = connectionToClient.identity.GetComponent<RTSPlayer>();

        teamColor = player.GetTeamColor();
    }

    #endregion

    #region Client

    private void ClientHandleTeamColorUpdate(Color oldColor, Color newColor)
    {
        if (!hasAuthority) return;

        if (null == minimap_renderer) return;

        List<Material> materials = new List<Material>();
        minimap_renderer.GetMaterials(materials);

        foreach (Material material in materials)
        {
            material.SetColor("_BaseColor", newColor);
        }
    }

    #endregion
}
