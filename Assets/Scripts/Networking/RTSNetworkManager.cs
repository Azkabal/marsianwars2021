using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RTSNetworkManager : NetworkManager //inherit network manager (using all of functions with ability to override virtual functions)
{
    [Scene]
    [SerializeField] string testScene;

    [SerializeField] GameObject unitBase_prefab = null;
    [SerializeField] GameOverHandler gameOverHandlerPrefab = null; 
    [SerializeField] float zOffsetOnStart = 20f;

    public static event Action ClientOnConnected;
    public static event Action ClientOnDisconnected;

    private bool isGameInProgress = false;
    public List<RTSPlayer> players { get; } = new List<RTSPlayer>();

    #region Server

    public override void OnServerConnect(NetworkConnection conn)
    {
        if(!isGameInProgress) { return; }

        conn.Disconnect();
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        RTSPlayer player = conn.identity.GetComponent<RTSPlayer>();

        players.Remove(player);

        base.OnServerDisconnect(conn);
    }

    public override void OnStopServer()
    {
        players.Clear();

        isGameInProgress = false;
    }

    public void StartGame()
    {
        //TODO: create 2 different network managers -> one for campaign mode, one for multiplayer mode
        // OR create a switch system to get network manager know what is currently to be played
        //if(players.Count < 1) { return; }
        Debug.Log("Starting game on scene : " + testScene);

        isGameInProgress = true;

        ServerChangeScene(testScene);
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        base.OnServerAddPlayer(conn);

        RTSPlayer player = conn.identity.GetComponent<RTSPlayer>();

        players.Add(player);

        player.SetTeamColor(new Color
            (
            UnityEngine.Random.Range(0f, 1f),
            UnityEngine.Random.Range(0f, 1f),
            UnityEngine.Random.Range(0f, 1f)
            ));

        player.SetIsPartyOwner(players.Count == 1);
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        GameOverHandler gameOverHandlerInstance =
            Instantiate(gameOverHandlerPrefab);

        NetworkServer.Spawn(gameOverHandlerInstance.gameObject);

        foreach (RTSPlayer player in players)
        {
            Vector3 startPosition = GetStartPosition().position;
            GameObject baseInstance = Instantiate(
               unitBase_prefab,
               startPosition,
               Quaternion.identity);

            NetworkServer.Spawn(
                baseInstance,
                player.connectionToClient);

            player.GetCameraTransform().position = new Vector3(startPosition.x, player.GetCameraTransform().position.y, startPosition.z - zOffsetOnStart);
        }
    }
    #endregion

    #region Client

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        Debug.Log("Client connected");
        ClientOnConnected?.Invoke();
    }
    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);

        ClientOnDisconnected?.Invoke();
    }

    public override void OnStopClient()
    {
        players.Clear();
    }
    #endregion
}
