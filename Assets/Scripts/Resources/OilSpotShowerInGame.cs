using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilSpotShowerInGame : MonoBehaviour
{
    [SerializeField] SphereCollider myCollider = null;
    [SerializeField] GameObject oilSphere = null;
    [SerializeField] GameObject oilSphereMinimap = null;

    private void Start()
    {
        float radius = myCollider.radius;

        oilSphereMinimap.transform.localScale = new Vector3(radius * 2, radius * 2, radius * 2);
        oilSphere.transform.localScale = new Vector3(radius * 2, radius * 2, radius * 2);

        oilSphere.SetActive(false);

        BuildingButton.ClientShowResources += ClientShowResources;
        BuildingButton.ClientHideResources += ClientHideResources;
    }

    private void OnDestroy()
    {
        BuildingButton.ClientShowResources -= ClientShowResources;
        BuildingButton.ClientHideResources -= ClientHideResources;
    }

    private void ClientShowResources()
    {
        oilSphere.SetActive(true);
    }
    private void ClientHideResources()
    {
        oilSphere.SetActive(false);
    }

    public float GetRadius()
    {
        return myCollider.radius;
    }
}
