using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResearchDisplay : MonoBehaviour
{
    [SerializeField] private TMP_Text researchText = null;

    private RTSPlayer player;

    private void Start()
    {
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();

        if (player != null)
        {
            ClientHandleResearchUpdated(player.GetResearchPoints());

            player.ClientOnResearchUpdated += ClientHandleResearchUpdated;
        }
    }

    private void OnDestroy()
    {
        player.ClientOnResearchUpdated -= ClientHandleResearchUpdated;
    }

    private void ClientHandleResearchUpdated(int points)
    {
        researchText.text = ": " + points;
    }
}
