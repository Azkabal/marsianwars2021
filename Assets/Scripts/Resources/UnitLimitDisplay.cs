using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UnitLimitDisplay : MonoBehaviour
{
    [SerializeField] private TMP_Text unitLimitText = null;
    private RTSPlayer player;
    private RTSPlayerResources locPlayerResources;
    // Start is called before the first frame update
    void Start()
    {
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();

        if(player != null)
        {
            locPlayerResources = player.playerResources;
        }
    }

    private void FixedUpdate()
    {
        unitLimitText.text = locPlayerResources.GetCurrentEnergy().ToString() + "/" + locPlayerResources.GetEnergyLimit().ToString();
    }
}
