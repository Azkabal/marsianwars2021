using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceDisplay : MonoBehaviour
{
    [SerializeField] private TMP_Text resourcesText = null;

    private RTSPlayer player;
    private RTSPlayerResources locPlayerResources;

    private void Start()
    {
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();
        if (player != null)
        {
            locPlayerResources = player.playerResources;
            ClientHandleResourcesUpdated(locPlayerResources.GetResources(), locPlayerResources.GetResourcesMaxCapacity());

            locPlayerResources.ClientOnResourcesUpdated += ClientHandleResourcesUpdated;
        }
    }

    private void OnDestroy()
    {
        locPlayerResources.ClientOnResourcesUpdated -= ClientHandleResourcesUpdated;
    }

    private void ClientHandleResourcesUpdated(int resources, int maxCapacity)
    {
        resourcesText.text = ": " + resources + " / " + maxCapacity;
    }
}
