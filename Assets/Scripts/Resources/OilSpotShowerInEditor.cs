using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class OilSpotShowerInEditor : MonoBehaviour
{
    [SerializeField] SphereCollider myCollider;

    void OnDrawGizmos()
    {
        float radius = myCollider.radius;

        Gizmos.color = new Color(
            0.6f,
            0.6f,
            0.6f,
            0.6f);
        Gizmos.DrawSphere(
            transform.position,
            radius);
    }
}
