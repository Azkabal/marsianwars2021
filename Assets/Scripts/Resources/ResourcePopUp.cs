using UnityEngine;
using TMPro;
using Mirror;

public class ResourcePopUp : MonoBehaviour
{
    [SerializeField] private TMP_Text popupText = null;
    [SerializeField] private float destroyTime = 4f;

    //[SyncVar(hook = nameof(NumberOfResourceChanged))]
    int numberOfResource = 10;

    public void ClientSetPopupValue(int newValue)
    {
        numberOfResource = newValue;
        popupText.text = "+" + numberOfResource.ToString();
    }

    private void Start()
    {
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y + 1f,
            transform.position.z
            );
        Invoke("DestroyMe", destroyTime);
    }

    private void Update()
    {
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y + 1f*Time.deltaTime,
            transform.position.z
            );
    }

    private void DestroyMe()
    {
        Destroy(gameObject);
    }

    /*
    private void NumberOfResourceChanged(int oldR, int newR)
    {
        popupText.text = "+" + newR.ToString();
    }
    */


}
