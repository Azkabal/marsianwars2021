using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingRotatorVX : MonoBehaviour
{
    [SerializeField] private Vector3 rotationVector;
    [SerializeField] private float maxRotationSpeed;


    private void LateUpdate()
    {
        transform.Rotate(rotationVector, maxRotationSpeed * Time.deltaTime);
    }
}
