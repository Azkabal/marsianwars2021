using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingRotatorVXTriggered : MonoBehaviour
{
    private enum RotationState
        {
        idle,
        startRotation,
        Rotation,
        endRotation,
    };

    [Header("References")]
    [SerializeField] private Transform objectToRotate = null;
    [Header("Settings")]
    [SerializeField] private Vector3 rotationVector;
    [Range(10f, 40f)]
    [SerializeField] private float maxRotationSpeed;
    [Range(1f,10f)]
    [SerializeField] private float changeRotationSpeed = 2f;

    private RotationState rotationState = RotationState.idle;
    private float lastRotationSpeed = 0f;
    private float nextMaxRotationSpeed = 0f;
    private bool rotationActive = false;

    public void SetRotation(bool newRotationBool)
    {
        rotationActive = newRotationBool;
    }

    private void Update()
    {
        switch(rotationState)
        {
            case RotationState.idle:
                if (rotationActive)
                {
                    rotationState = RotationState.startRotation;
                }
                break;
            case RotationState.startRotation:
                if (rotationActive)
                {
                    if(lastRotationSpeed < maxRotationSpeed)
                    {
                        lastRotationSpeed += changeRotationSpeed;
                        RotateWithSpeed(lastRotationSpeed);
                    }
                    else
                    {
                        nextMaxRotationSpeed = Random.Range(maxRotationSpeed - 5f, maxRotationSpeed + 5f);
                        rotationState = RotationState.Rotation;
                    }
                }
                else
                {
                    rotationState = RotationState.endRotation;
                }
                break;
            case RotationState.Rotation:
                if (rotationActive)
                {
                    RotateWithSpeed(nextMaxRotationSpeed);
                }
                else
                {
                    rotationState = RotationState.endRotation;
                }
                break;
            case RotationState.endRotation:
                if (!rotationActive)
                {
                    if (lastRotationSpeed >= changeRotationSpeed)
                    {
                        lastRotationSpeed -= changeRotationSpeed;
                        RotateWithSpeed(lastRotationSpeed);
                    }
                    else
                    {
                        rotationState = RotationState.idle;
                    }
                }
                else
                {
                    rotationState = RotationState.startRotation;
                }
                break;
        }


    }

    private void RotateWithSpeed(float frameRotationSpeed)
    {
        objectToRotate.transform.Rotate(rotationVector, frameRotationSpeed * Time.deltaTime);
    }
}
