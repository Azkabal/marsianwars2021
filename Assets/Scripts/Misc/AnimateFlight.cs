using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateFlight : MonoBehaviour
{
    [SerializeField] private float Range = 0.5f;
    [SerializeField] private float Speed = 0.5f;
    [SerializeField] private float RandomIndexUp = 1f;
    [SerializeField] private float RandomIndexDown = 0.5f;

    private float minY = 0f;
    private float maxY = 0f;
    private bool goingUp = false;

    private void Awake()
    {
        minY = transform.position.y - Range;
        maxY = transform.position.y + Range;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float currY = transform.position.y;

        if(goingUp)
        {
            currY += Speed * Random.Range(0, RandomIndexUp) * Time.deltaTime;

            if (currY > maxY)
            {
                goingUp = false;
            }
        }
        else
        {
            currY -= Speed * Random.Range(0, RandomIndexDown) * Time.deltaTime;

            if (currY < minY)
            {
                goingUp = true;
            }
        }

        transform.position = new Vector3(transform.position.x, currY, transform.position.z);
    }
}
