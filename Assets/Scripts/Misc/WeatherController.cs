using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : NetworkBehaviour
{
    [SerializeField] private ParticleSystem SandStormBigRef = null;
    [SerializeField] private ParticleSystem SandStormSmallRef = null;

    [SerializeField] private bool isStrongStormActive = false;

    private float weatherTimer = 0f;
    [SerializeField] float minWeatherChangeTime = 20f;
    [SerializeField] float addRandomWeatherChangeTime = 10f;
    private float nextWeatherTime = 0f;

    public Action<bool> ServerOnWeatherChanged;

    private ParticleSystem.EmissionModule BigEmitter;
    private ParticleSystem.EmissionModule SmallEmitter;

    public bool GetStormStatus()
    {
        return isStrongStormActive;
    }

    // Start is called before the first frame update
    public override void OnStartServer()
    {
        SetNextWeatherTime();
        if (SandStormBigRef == null)
        {
            Debug.LogError("SandStormBigRef == null!");
            return;
        }
        if (SandStormSmallRef == null)
        {
            Debug.LogError("SandStormSmallRef == null!");
            return;
        }
        BigEmitter = SandStormBigRef.emission;
        SmallEmitter = SandStormSmallRef.emission;

        ApplySandStormValue();
    }

    private void SetNextWeatherTime()
    {
        nextWeatherTime = minWeatherChangeTime + UnityEngine.Random.Range(0f, addRandomWeatherChangeTime);
    }

    [ServerCallback]
    private void Update()
    {
        weatherTimer += Time.deltaTime;

        if(weatherTimer > nextWeatherTime)
        {
            weatherTimer = 0f;
            ChangeSandStorm(!isStrongStormActive);
        }
    }

    public void ChangeSandStorm(bool sandStormValue)
    {
        isStrongStormActive = sandStormValue;

        ApplySandStormValue();

        ServerOnWeatherChanged?.Invoke(isStrongStormActive);

        SetNextWeatherTime();
    }

    private void ApplySandStormValue()
    {
        BigEmitter.enabled = (isStrongStormActive);
        SmallEmitter.enabled = (!isStrongStormActive);
    }
}
