using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnitSpawner : NetworkBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Building myBuilding = null;
    [SerializeField] private Health health = null;
    [SerializeField] private Unit unit_prefab = null;
    [SerializeField] private Transform spawnPosition = null;

    [SerializeField] private GameObject spawnerDisplay = null;
    [SerializeField] private TMP_Text remainingUnitsText = null;
    [SerializeField] private Image unitProgressImage = null;
    [SerializeField] private int maxUnitQueue = 5;
    [SerializeField] private float spawnMoveRange = 7f;
    [SerializeField] private float unitSpawnDuration = 5f;

    [SyncVar(hook=nameof(ClienHandleQueuedUnitsUpdated))]
    private int queuedUnits;
    [SyncVar]
    private float unitTimer;

    private float progressImageVelocity;

    private RTSPlayer player = null; 
    private RTSPlayerResources locPlayerResources = null;

    private void Start()
    {
        if(isServer)
        {
            player =
                connectionToClient.identity.GetComponent<RTSPlayer>();

            if (player != null)
            {
                locPlayerResources = player.playerResources;
            }
            return;
        }

        if(isClientOnly)
        {
            player =
                NetworkClient.connection.identity.GetComponent<RTSPlayer>();

            if (player != null)
            {
                locPlayerResources = player.playerResources;
            }
            return;
        }
    }

    private void Update()
    {
        if (!myBuilding.IsFinished()) return;

        if(isServer)
        {
            ProduceUnits();
        }

        if(isClient)
        {
            UpdateTimerDisplay();
        }
    }

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    [Command]
    private void CmdSpawnUnit()
    {
        if(queuedUnits == maxUnitQueue) { return; }

        if (locPlayerResources.GetResources() < unit_prefab.GetResourceCost()) return;

        if (locPlayerResources.GetCurrentEnergy() < unit_prefab.GetEnergyCost()) return;

        queuedUnits++;

        locPlayerResources.ServerSetResources(
            locPlayerResources.GetResources() - unit_prefab.GetResourceCost());

        locPlayerResources.ServerSetEnergy(
            locPlayerResources.GetCurrentEnergy() - unit_prefab.GetEnergyCost());
    }

    [Server]
    private void ProduceUnits()
    {
        if (player.GetMyUnits().Count >= locPlayerResources.GetEnergyLimit()) return;

        if (queuedUnits == 0) return;

        unitTimer += Time.deltaTime;

        if (unitTimer < unitSpawnDuration) return;

        GameObject unitInstance = Instantiate(
            unit_prefab.gameObject,
            spawnPosition.position,
            spawnPosition.rotation);

        NetworkServer.Spawn(unitInstance, connectionToClient);

        Vector3 spawnOffset = UnityEngine.Random.insideUnitSphere * spawnMoveRange;
        spawnOffset.y = spawnPosition.position.y;

        UnitMovement unitMovement = unitInstance.GetComponent<UnitMovement>();
        unitMovement.ServerMove(spawnPosition.position + spawnOffset);

        queuedUnits--;

        unitTimer = 0f;
    }

    #endregion



    #region Client

    [Client]
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        if(!hasAuthority) return;

        CmdSpawnUnit();
    }
    public void ClienHandleQueuedUnitsUpdated(int oldUnits, int newUnits)
    {
        remainingUnitsText.text = newUnits.ToString();
    }

    private void UpdateTimerDisplay()
    {
        float newProgress = unitTimer / unitSpawnDuration;

        if(newProgress < unitProgressImage.fillAmount)
        {
            unitProgressImage.fillAmount = newProgress;
        }
        else
        {
            unitProgressImage.fillAmount = Mathf.SmoothDamp(
                unitProgressImage.fillAmount,
                newProgress,
                ref progressImageVelocity,
                0.1f
                );
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        spawnerDisplay.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        spawnerDisplay.SetActive(false);
    }

    #endregion
}
