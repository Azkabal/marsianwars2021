using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnergyGenerator : NetworkBehaviour
{
    public enum energySetting
    {
        normalGenerator,
        stormGenerator,
        universalGenerator,
    };

    [SerializeField] private Building myBuilding = null;
    [SerializeField] private Health health = null;

    [Header("Energy generation settings")]
    [SerializeField] private int energyGenerated = 10;
    [SerializeField] private energySetting generatorType = energySetting.normalGenerator;
    [SerializeField] private WingRotatorVXTriggered rotator = null;
    
    private bool activeGeneration = false;
    
    private RTSPlayer player = null;
    private RTSPlayerResources locPlayerResources = null;

    private WeatherController weatherController = null;

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;

        player = connectionToClient.identity.GetComponent<RTSPlayer>();

        if(player != null)
        {
            locPlayerResources = player.playerResources;
        }

        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;

        myBuilding.ServerOnBuildingFinished += ServerOnBuildingFinished;

        //find weather controller
        weatherController = FindObjectOfType<WeatherController>();
        if(weatherController == null)
        {
            Debug.LogError("Weather controller not found!");
            return;
        }
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
        myBuilding.ServerOnBuildingFinished -= ServerOnBuildingFinished;
    }

    private void ServerOnWeatherChanged(bool stromStatus)
    {
        bool generate = true;
        switch (generatorType)
        {
            case energySetting.normalGenerator:
                {
                    if (stromStatus)
                    {
                        generate = false;
                    }
                    else
                    {
                        generate = true;
                    }
                    break;
                }
            case energySetting.stormGenerator:
                {
                    if (stromStatus)
                    {
                        generate = true;
                    }
                    else
                    {
                        generate = false;
                    }
                    break;
                }
            case energySetting.universalGenerator:
                {
                    generate = true;
                    break;
                }
            default:
                {
                    Debug.LogError("wrong format");
                    generate = false;
                    break;
                }
        }

        ChargeDischarge(generate);

        if (rotator != null)
        {
            CallClientRPCRotatorSetRotation(activeGeneration);
        }
    }

    private void ChargeDischarge(bool generate)
    {
        if (player == null) return;

        if (generate == activeGeneration) return;

        activeGeneration = generate;

        if (activeGeneration)
        {
            locPlayerResources.CmdSetEnergy(locPlayerResources.GetCurrentEnergy() + energyGenerated);
        }
        else
        {
            locPlayerResources.CmdSetEnergy(locPlayerResources.GetCurrentEnergy() - energyGenerated);
        }
    }

    [ClientRpc]
    private void CallClientRPCRotatorSetRotation(bool active)
    {
        rotator.SetRotation(active);
    }

    private void ServerOnBuildingFinished()
    {
        weatherController.ServerOnWeatherChanged += ServerOnWeatherChanged;

        ServerOnWeatherChanged(weatherController.GetStormStatus());
    }

    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    private void ServerHandleGameOver()
    {
        activeGeneration = false;
        enabled = false;
    }
    
    #endregion

}
