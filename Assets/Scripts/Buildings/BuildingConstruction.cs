using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingConstruction : MonoBehaviour
{
    [SerializeField] private Building myBuilding = null;
    [SerializeField] private float effectTime = 1f;
    [SerializeField] private GameObject BuildVFX_prefab = null;

    private float effectTimer = 0f;

    private void Update()
    {
        if (BuildVFX_prefab == null) return;

        if (myBuilding.IsFinished()) return;

        if (effectTimer > effectTime)
        {
            effectTimer = 0f;

            //create VFX
            GameObject vfx_effect = Instantiate(BuildVFX_prefab, this.transform.position, Quaternion.identity);
        }

        effectTimer += Time.deltaTime;
    }
}
