using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombDetonator : NetworkBehaviour
{
    [SerializeField] private Building myBuilding = null;
    [SerializeField] private LayerMask layerMaks = new LayerMask();
    [SerializeField] private Health health = null;
    [SerializeField] private float bombRadius = 2f;
    [SerializeField] private int bombDamage = 50;

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if(!myBuilding.IsFinished()) return;

        if (other == null) return;

        if (!other.gameObject.TryGetComponent<NetworkIdentity>(out NetworkIdentity id)) return;

        if(other.gameObject.GetComponent<AlienMovementController>() == null)
        {
            if (id.connectionToClient == this.connectionToClient) return;
        }

        if (other.gameObject.TryGetComponent<Health>(out Health enemyHealth))
        {
            Collider[] colliders = Physics.OverlapSphere(
                transform.position,
                bombRadius,
                layerMaks);

            foreach(Collider collider in colliders)
            {
                if (!collider.TryGetComponent<Health>(out Health colliderHealth)) continue;

                colliderHealth.DealDamage(bombDamage);
            }

            enemyHealth.DealDamage(bombDamage);
            health.DealDamage(1000);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(
            transform.position,
            bombRadius
            );
    }

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    #endregion
}
