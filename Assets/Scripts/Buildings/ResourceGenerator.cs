using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceGenerator : NetworkBehaviour
{
    [SerializeField] private Building myBuilding = null;
    [SerializeField] private Health health = null;
    [SerializeField] private int resourcesPerInterval = 10;
    [SerializeField] private float interval = 1f;

    private float timer = 0f;
    private RTSPlayer player;
    private RTSPlayerResources locPlayerResources = null;

    private bool activeGeneration = false;

    [SyncVar]
    private int oilResources = 0;

    [Server]
    private void ServerHandleOnResourcesFlowChanged(int newResources)
    {
        oilResources = newResources;
    }

    public override void OnStartServer()
    {
        timer = interval;

        health.ServerOnDie += ServerHandleDie;

        player = connectionToClient.identity.GetComponent<RTSPlayer>();

        if(player != null)
        {
            player.ServerOnResourcesFlowChanged += ServerHandleOnResourcesFlowChanged;
            locPlayerResources = player.playerResources;

            GameOverHandler.ServerOnGameOver += ServerHandleGameOver;

            myBuilding.ServerOnBuildingFinished += ServerOnBuildingFinished;

            oilResources = player.GetUpdateResources();
        }
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
        player.ServerOnResourcesFlowChanged -= ServerHandleOnResourcesFlowChanged;
        myBuilding.ServerOnBuildingFinished -= ServerOnBuildingFinished;
    }

    private void ServerOnBuildingFinished()
    {
        activeGeneration = true;
    }

    [ServerCallback]
    private void Update()
    {
        if (!activeGeneration) return;

        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            int resourcesGained = resourcesPerInterval + oilResources;
            
            locPlayerResources.ServerSetResources(locPlayerResources.GetResources() + resourcesGained); 

            timer += interval;
        }
    }

    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    private void ServerHandleGameOver()
    {
        enabled = false;
    }
}
