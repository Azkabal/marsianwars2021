using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResearchProduction : NetworkBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Building myBuilding = null;
    [SerializeField] private Health health = null;
    [SerializeField] private TMP_Text remainingResearchText = null;
    [SerializeField] private Image researchProgressImage = null; 
    [SerializeField] private GameObject researchDisplay = null;


    [SerializeField] private int researchPrice = 1000;
    [SerializeField] private float researchDuration = 3f * 60f;
    [SerializeField] private int maxResearchQueue = 3;

    [SyncVar(hook = nameof(ClienHandleQueuedResearchUpdated))]
    private int queuedResearch;
    [SyncVar]
    private float researchTimer;

    private float progressImageVelocity;

    private RTSPlayer player;
    private RTSPlayerResources locPlayerResources = null;

    private void Update()
    {
        if (!myBuilding.IsFinished()) return;

        if (isServer)
        {
            ProduceResearch();
        }

        if (isClient)
        {
            UpdateTimerDisplay();
        }
    }

    [Command]
    private void CmdStartResearch()
    {
        if (queuedResearch == maxResearchQueue) { return; }

        if (locPlayerResources.GetResources() < researchPrice) return;

        queuedResearch++;

        locPlayerResources.ServerSetResources(
            locPlayerResources.GetResources() - researchPrice);
    }

    #region Server

    public override void OnStartServer()
    {
        player =
            connectionToClient.identity.GetComponent<RTSPlayer>();

        if(player != null)
        {
            locPlayerResources = player.playerResources;
        }

        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    [Server]
    private void ProduceResearch()
    {
        if (queuedResearch == 0) return;

        researchTimer += Time.deltaTime;

        if (researchTimer < researchDuration) return;

        player.SetResearchPoints(player.GetResearchPoints() + 1);

        queuedResearch--;

        researchTimer = 0f;
    }

    #endregion

    #region Client

    [Client]
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        if (!hasAuthority) return;

        CmdStartResearch();
    }
    public void ClienHandleQueuedResearchUpdated(int oldResearch, int newResearch)
    {
        remainingResearchText.text = newResearch.ToString();
    }

    private void UpdateTimerDisplay()
    {
        float newProgress = researchTimer / researchDuration;

        if (newProgress < researchProgressImage.fillAmount)
        {
            researchProgressImage.fillAmount = newProgress;
        }
        else
        {
            researchProgressImage.fillAmount = Mathf.SmoothDamp(
                researchProgressImage.fillAmount,
                newProgress,
                ref progressImageVelocity,
                0.1f
                );
        }
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        researchDisplay.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        researchDisplay.SetActive(false);
    }

    #endregion


}
