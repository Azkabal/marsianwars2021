using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Building building = null;
    [SerializeField] private Image iconImage = null;
    [SerializeField] private TMP_Text priceText = null;
    [SerializeField] private LayerMask floorMask = new LayerMask();

    [SerializeField] private string buildingName = null;
    [SerializeField] private string buildingDescription = null;
    [SerializeField] private GameObject descriptionPanel = null;
    [SerializeField] private TMP_Text descriptionText = null;
    [SerializeField] private float BuildingPreviewRotationSpeed = 5f;

    private BoxCollider buildingCollider = null; 

    private Camera mainCamera;
    private RTSPlayer player = null; 
    private RTSPlayerResources locPlayerResources = null;
    private UnitSelectionHandler unitSelectionHandler;
    private GameObject buildingPreviewInstance;
    private Renderer[] buildingRenderersInInstance;

    public static event Action ClientShowResources;
    public static event Action ClientHideResources;

    private float LastRotYAxis = 0f;

    private void Start()
    {
        descriptionPanel.SetActive(false);
        descriptionText.text = buildingName + "\n" + buildingDescription;

        mainCamera = Camera.main;

        iconImage.sprite = building.GetIcon();
        priceText.text = building.GetPrice().ToString();

        buildingCollider = building.GetComponent<BoxCollider>();

        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();

        if(player == null)
        {
            Debug.LogError("Failed to get reference to player object");
        }
        else
        {
            locPlayerResources = player.playerResources;
        }

        unitSelectionHandler = FindObjectOfType<UnitSelectionHandler>();

        player.ClientOnBombCostUpdatedUpdated += UpdatePriceText;
    }

    private void OnDestroy()
    {
        player.ClientOnBombCostUpdatedUpdated -= UpdatePriceText;
    }

    private void UpdatePriceText()
    {
        Debug.Log("UpdatePriceText is called!");
        if (building.GetId() == 5) //todo : make this cleaner!
            priceText.text = (building.GetPrice() - player.GetReduceBombCost()).ToString();


        Debug.Log("Bomb cost is now " + player.GetReduceBombCost());
    }

    private void Update()
    {
        if(buildingPreviewInstance == null) { return; }
        
        UpdateBuildingPreview();
    }

    private void UpdateBuildingRotation()
    {
        if (Keyboard.current.eKey.isPressed)
        {
            LastRotYAxis -= BuildingPreviewRotationSpeed * Time.deltaTime;
        }
        else if (Keyboard.current.qKey.isPressed)
        {
            LastRotYAxis += BuildingPreviewRotationSpeed * Time.deltaTime;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(eventData.button != PointerEventData.InputButton.Left) { return; }

        if (building.GetId() == 5) //todo : make this cleaner!
        {
            if (locPlayerResources.GetResources() < (building.GetPrice() - player.GetReduceBombCost())) return;
        }
        else
        {
            if (locPlayerResources.GetResources() < building.GetPrice()) return;
        }
        if (locPlayerResources.GetCurrentEnergy() < (building.GetEnergy())) return;

        buildingPreviewInstance = Instantiate(building.GetBuildingPreview());
        buildingRenderersInInstance = buildingPreviewInstance.GetComponentsInChildren<Renderer>();

        buildingPreviewInstance.SetActive(false);

        unitSelectionHandler.SetIsBuilding(true);

        ClientShowResources?.Invoke();

        LastRotYAxis = 0f;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(buildingPreviewInstance == null) { return; }

        Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

        if(Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, floorMask))
        {
            player.CmdTryPlaceBuilding(building.GetId(), hit.point, LastRotYAxis);
        }

        Destroy(buildingPreviewInstance);

        unitSelectionHandler.SetIsBuilding(false);

        ClientHideResources?.Invoke();

        LastRotYAxis = 0f;
    }

    private void UpdateBuildingPreview()
    {
        Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

        if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, floorMask))
        {
            return;
        }

        buildingPreviewInstance.transform.position = hit.point;

        if(!buildingPreviewInstance.activeSelf)
        {
            buildingPreviewInstance.SetActive(true);
        }
        else
        {
            UpdateBuildingRotation();

            buildingPreviewInstance.transform.rotation = Quaternion.Euler(0, LastRotYAxis, 0);
        }

        bool isGenerator = (building.GetId() == 2); //TODO: find better way to determine different buildings types

        Color color;
        if (isGenerator) //resource generator
        {
            color =
                player.CanPlaceGenerator(
                    buildingCollider, hit.point, LastRotYAxis) ? Color.green : Color.red;
        }
        else
        {
            color =
                player.CanPlaceBuilding(
                    buildingCollider, hit.point, LastRotYAxis) ? Color.green : Color.red;
        }

        foreach(Renderer renderer in buildingRenderersInInstance)
        {
            //update renderer colors
            renderer.material.SetColor("_BaseColor", color);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        descriptionPanel.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        descriptionPanel.SetActive(false);
    }
}
