using Mirror;
using UnityEngine;

public class DefenceTower : NetworkBehaviour
{
    [SerializeField] private Building myBuilding = null;
    [SerializeField] private Health health = null;
    [SerializeField] private Targeter targeter = null;
    [SerializeField] private LineOfSight lineOfSight = null;

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    [ServerCallback]

    private void Update()
    {
        if (!myBuilding.IsFinished()) return;

        if (targeter.GetTarget() != null) return;

        Targetable possibleTarget = lineOfSight.GetTargetInSight();

        if (possibleTarget != null)
        {
            ServerTrySetTarget(possibleTarget.gameObject);
        }
    }

    [Server]
    private void ServerTrySetTarget(GameObject possibleTarget)
    {
        if (possibleTarget == null) return;
        if (possibleTarget.GetComponent<Targetable>() == null) return;

        targeter.ServerSetTarget(possibleTarget);
    }
    #endregion

    #region Client

    #endregion
}
