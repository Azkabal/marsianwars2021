using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderBuilding : NetworkBehaviour
{
    [SerializeField] private Health health = null;

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    [Server]
    private void ServerHandleDie()
    {
        NetworkServer.Destroy(gameObject);
    }

    #endregion
}
