using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : NetworkBehaviour
{
    [Header("references")]
    [SerializeField] private Health health = null;
    [SerializeField] private GameObject buildingPreview = null;
    [Header("UI settings")]
    [SerializeField] private Sprite icon = null;
    [SerializeField] private int id = -1;
    [Header("building settings")]
    [SerializeField] private int price = 100;
    [SerializeField] private int energy = 3;
    [Tooltip("In seconds")]
    [SerializeField] private float buildTime = 5f;

    public event Action ServerOnBuildingFinished;

    public static event Action<Building> ServerOnBuildingSpawned;
    public static event Action<Building> ServerOnBuildingDespawned;

    public static event Action<Building> AuthorityOnBuildingSpawned;
    public static event Action<Building> AuthorityOnBuildingDespawned;

    private float buildTimer = 0f;
    private float ProggressTimer = 0f;
    private bool isFinished = false;

    private RTSPlayer player = null;
    private RTSPlayerResources locPlayerResources = null;

    public GameObject GetBuildingPreview()
    {
        return buildingPreview;
    }

    public Sprite GetIcon()
    {
        return icon;
    }
    public int GetId()
    {
        return id;
    }
    public int GetPrice()
    {
        return price;
    }
    public int GetEnergy()
    {
        return energy;
    }
    public float GetBuildTime()
    {
        return buildTime;
    }
    public bool IsFinished()
    {
        return isFinished;
    }

    private void Update()
    {
        if(!isFinished)
        {
            buildTimer += Time.deltaTime;

            if (isServer)
            {
                ProggressTimer += Time.deltaTime;

                if (ProggressTimer >= 1f)
                {
                    int healthProgress = (int)(ProggressTimer / buildTime * health.GetMaxHealth() * 110 / 100);
                    health.AddHealth(healthProgress);

                    ProggressTimer = 0f;
                }
            }

            if (buildTimer > buildTime)
            {
                isFinished = true;
                ServerOnBuildingFinished?.Invoke();

                //TODO: energy consumer script?
                if (locPlayerResources != null)
                {
                    if (!isServer) return;

                    //allow call only from server
                    locPlayerResources.ServerSetEnergy(locPlayerResources.GetCurrentEnergy() - energy);
                }
            }
        }


    }

    #region Server

    public override void OnStartServer()
    {
        player = connectionToClient.identity.GetComponent<RTSPlayer>();

        if(player != null)
        {
            locPlayerResources = player.playerResources;
        } 

        ServerOnBuildingSpawned?.Invoke(this);
    }

    public override void OnStopServer()
    {
        ServerOnBuildingDespawned?.Invoke(this);
    }

    #endregion

    #region Client

    public override void OnStartAuthority()
    {
        AuthorityOnBuildingSpawned?.Invoke(this);
    }

    public override void OnStopClient()
    {
        AuthorityOnBuildingDespawned?.Invoke(this);
    }

    #endregion
}
