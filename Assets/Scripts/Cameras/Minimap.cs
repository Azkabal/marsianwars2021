using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class Minimap : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [SerializeField] private RectTransform minimapRect = null;
    [SerializeField] private RectTransform scaledRect = null;
    [SerializeField] private float mapScale = 20f;
    [SerializeField] private float offsetX = 10f;
    [SerializeField] private float offsetZ = 15f;

    private Transform playerCameraTransform;
    private UnitSelectionHandler unitSelectionHandler;

    private bool isScaled = false;
    private float buttonPressedTimeout = 0.2f;
    private float buttonPressedTimer = 0f;


    private void Start()
    {
        if (NetworkClient.connection.identity == null) { return; }

        playerCameraTransform =
            NetworkClient.connection.identity.GetComponent<RTSPlayer>().GetCameraTransform();

        unitSelectionHandler = FindObjectOfType<UnitSelectionHandler>();
    }

    private void Update()
    {
        buttonPressedTimer += Time.deltaTime;

        if (buttonPressedTimer < buttonPressedTimeout) return;

        if (Keyboard.current.mKey.isPressed)
        {
            buttonPressedTimer = 0f;
            if (isScaled)
            {
                ScaleDown();
            }
            else
            {
                ScaleUp();
            }
            isScaled = !isScaled;
            return;
        }


    }

    private void ScaleUp()
    {
        scaledRect.sizeDelta = new Vector2(scaledRect.sizeDelta.x * 2f, scaledRect.sizeDelta.y * 2f);
    }
    private void ScaleDown()
    {
        scaledRect.sizeDelta = new Vector2(scaledRect.sizeDelta.x / 2f, scaledRect.sizeDelta.y / 2f);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        MoveCamera();

        unitSelectionHandler.SetIsMinimap(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        unitSelectionHandler.SetIsMinimap(false);
    }
    public void OnDrag(PointerEventData eventData)
    {
        MoveCamera();
    }

    private void MoveCamera()
    {
        Vector2 mousePos = Mouse.current.position.ReadValue();

        if(!RectTransformUtility.ScreenPointToLocalPointInRectangle(
            minimapRect,
            mousePos,
            null,
            out Vector2 localPoint
            ))
        {
            return;
        }

        Vector2 lerp = new Vector2(
            ((localPoint.x - minimapRect.rect.x) / minimapRect.rect.width),
            ((localPoint.y - minimapRect.rect.y) / minimapRect.rect.height)
            );

        Vector3 newCameraPos = new Vector3(
            Mathf.Lerp(-mapScale, mapScale, lerp.x),
            playerCameraTransform.position.y,
            Mathf.Lerp(-mapScale, mapScale, lerp.y)
            );

        playerCameraTransform.position = 
            newCameraPos + new Vector3(offsetX, 0f, offsetZ);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }
}
