using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : NetworkBehaviour
{
    [SerializeField] private Transform playerCameraTransform = null;
    [SerializeField] private Cinemachine.CinemachineVirtualCamera playerCamera = null;
    [SerializeField] private float speed = 20f;
    [SerializeField] private float screenBorderThickness = 10f;
    [SerializeField] private Vector2 screenXLimits = Vector2.zero;
    [SerializeField] private Vector2 screenZLimits = Vector2.zero;
    [SerializeField] private float cameraZoomSpeed = 0.5f; 

    private Vector2 previousInput;
    private Vector2 previousZoom;

    private Controls controls;

    public override void OnStartAuthority()
    {
        playerCameraTransform.gameObject.SetActive(true);

        controls = new Controls();

        controls.Player.MoveCamera.performed += SetPreviousCameraInput;
        controls.Player.MoveCamera.canceled += SetPreviousCameraInput;

        controls.Player.Scroll.performed += SetPreviousCameraZoom;
        controls.Player.Scroll.canceled += SetPreviousCameraZoom;

        controls.Enable();
    }

    [ClientCallback]
    private void Update()
    {
        if(!hasAuthority || !Application.isFocused) { return; }

        UpdateCameraPosition();

        UpdateCameraZoom();
    }

    private void UpdateCameraPosition()
    {
        Vector3 pos = playerCameraTransform.position;

        if(previousInput == Vector2.zero)
        {
            Vector3 cursorMovement = Vector3.zero;

            Vector2 cursorPosition = Mouse.current.position.ReadValue();

            if(cursorPosition.y >= Screen.height - screenBorderThickness)
            {
                cursorMovement.z += 1;
            }
            else if(cursorPosition.y <= screenBorderThickness)
            {
                cursorMovement.z -= 1;
            }

            if (cursorPosition.x >= Screen.width - screenBorderThickness)
            {
                cursorMovement.x += 1;
            }
            else if (cursorPosition.x <= screenBorderThickness)
            {
                cursorMovement.x -= 1;
            }

            pos += cursorMovement.normalized * speed * Time.deltaTime;
        }
        else
        {
            pos += new Vector3(previousInput.x, 0f, previousInput.y) * speed * Time.deltaTime;
        }

        pos.x = Mathf.Clamp(pos.x, screenXLimits.x, screenXLimits.y);
        pos.z = Mathf.Clamp(pos.z, screenZLimits.x, screenZLimits.y);

        playerCameraTransform.position = pos;
    }

    private void UpdateCameraZoom()
    {
        if (previousZoom == Vector2.zero)
        {
            //do nothing?
        }
        else
        {
            float newFieldOfView = Mathf.Clamp(playerCamera.m_Lens.FieldOfView - previousZoom.y*cameraZoomSpeed*Time.deltaTime, 20f, 60f);

            playerCamera.m_Lens.FieldOfView = newFieldOfView;
        }
    }

    private void SetPreviousCameraInput(InputAction.CallbackContext ctx)
    {
        previousInput = ctx.ReadValue<Vector2>();
    }

    private void SetPreviousCameraZoom(InputAction.CallbackContext ctx)
    {
        previousZoom = ctx.ReadValue<Vector2>();
    }
}
