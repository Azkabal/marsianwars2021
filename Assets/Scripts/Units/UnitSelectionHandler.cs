using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitSelectionHandler : MonoBehaviour
{
    [SerializeField] LayerMask layerMask = new LayerMask();

    [SerializeField] private RectTransform unitSelectionArea = null;

    private int maxSelectedUnits = 100;

    private Vector2 startPosition;

    private RTSPlayer player;
    private Camera mainCamera;

    private bool isMinimap = false;
    private bool isBuilding = false;

    public event Action UnitsSelected;
    public event Action UnitsDeselected;

    public void SetIsMinimap(bool status)
    {
        isMinimap = status;
    }
    public void SetIsBuilding(bool status)
    {
        isBuilding = status;
    }

    public List<Unit> SelectedUnits { get; } = new List<Unit>(); //get but not set!

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();

        Unit.AuthorityOnUnitDespawned += AuthorityHandeUnitDespawned;
        GameOverHandler.ClientOnGameOver += ClientHandleGameOver;
    }

    private void OnDestroy()
    {
        Unit.AuthorityOnUnitDespawned -= AuthorityHandeUnitDespawned;
        GameOverHandler.ClientOnGameOver -= ClientHandleGameOver;
    }

    private void Update()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            StartSelectionArea();
        }
        else if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            ClearSelectionArea();
        }
        else if (Mouse.current.leftButton.isPressed)
        {
            UpdateSelectionArea();
        }
    }

    private void ClearUnitList()
    {
        if (SelectedUnits.Count != 0)
        {
            foreach (Unit selectedUnit in SelectedUnits)
            {
                selectedUnit.Deselect();
            }
            SelectedUnits.Clear();
        }

        UnitsDeselected?.Invoke();
    }

    private void ClearSelectionArea()
    {
        unitSelectionArea.gameObject.SetActive(false);

        if(unitSelectionArea.sizeDelta.magnitude == 0)
        {
            //single unit selection
            Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask)) { return; }

            if (!hit.collider.TryGetComponent(out Unit unit)) { return; }

            if (!unit.hasAuthority) { return; }

            if(!SelectedUnits.Contains(unit))
            {
                SelectedUnits.Add(unit);

                foreach (Unit selectedUnit in SelectedUnits)
                {
                    selectedUnit.Select();
                }
            }

            UnitsSelected?.Invoke();

            return;
        }

        //multi selection
        Vector2 min = unitSelectionArea.anchoredPosition - (unitSelectionArea.sizeDelta / 2);
        Vector2 max = unitSelectionArea.anchoredPosition + (unitSelectionArea.sizeDelta / 2);

        foreach(Unit unit in player.GetMyUnits())
        {
            Vector3 screenPosition = mainCamera.WorldToScreenPoint(unit.transform.position);

            if(screenPosition.x > min.x &&
               screenPosition.x < max.x &&
               screenPosition.y > min.y &&
               screenPosition.y < max.y)
            {
                if (SelectedUnits.Contains(unit))
                {
                    continue;
                }

                if (SelectedUnits.Count >= maxSelectedUnits) break;

                SelectedUnits.Add(unit);
                unit.Select();
            }
        }

        UnitsSelected?.Invoke();

    }

    private void StartSelectionArea()
    {
        if (isMinimap)
        {
            return; //ignore everything if it is called on minimap
        }

        if (isBuilding)
        {
            return; //ignore everything if it is called on building button
        }

        if (!Keyboard.current.leftShiftKey.isPressed)
        {
            ClearUnitList();
        }

        unitSelectionArea.gameObject.SetActive(true);

        startPosition = Mouse.current.position.ReadValue();

        UpdateSelectionArea();
    }
    private void UpdateSelectionArea()
    {
        Vector2 mousePosition = Mouse.current.position.ReadValue();

        float AreaWidth = mousePosition.x - startPosition.x;
        float AreaHeigth = mousePosition.y - startPosition.y;

        unitSelectionArea.sizeDelta = new Vector2(Mathf.Abs(AreaWidth), Mathf.Abs(AreaHeigth));
        unitSelectionArea.anchoredPosition = startPosition +
            new Vector2(AreaWidth / 2, AreaHeigth / 2);
    }

    private void AuthorityHandeUnitDespawned(Unit unit)
    {
        SelectedUnits.Remove(unit);
        UnitsSelected?.Invoke();
    }

    private void ClientHandleGameOver(string playerId)
    {
        enabled = false;
    }

    public void SortUnitsByType(int type)
    {
        ClearUnitList();

        List<Unit> playerUnits = player.GetMyUnits();

        foreach(Unit unit in playerUnits)
        {
            if(unit.GetUnitType() == type)
            {
                SelectedUnits.Add(unit);
                unit.Select();
            }
        }

        UnitsSelected?.Invoke();

    }
    public void SelectAllUnits()
    {
        ClearUnitList();

        List<Unit> playerUnits = player.GetMyUnits();

        foreach (Unit unit in playerUnits)
        {
            SelectedUnits.Add(unit);
            unit.Select();
        }

        UnitsSelected?.Invoke();

    }
}
