using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMeleeCombat : NetworkBehaviour
{
    [Header("References")]
    [SerializeField] private Health health = null;
    [SerializeField] private Targeter targeter = null;

    [Header("Combat")]
    [SerializeField] private int attackDamage = 50;
    [SerializeField] private float attackRange = 5f;
    [SerializeField] private float attackRate = 2f;
    [SerializeField] private float rotationSpeed = 180f;

    public event Action AnimationAttack;

    private float lastAttackTime = 0f;

    public float GetRotationSpeed()
    {
        return rotationSpeed;
    }
    public float GetAttackRange()
    {
        return attackRange;
    }

    [ServerCallback]
    private void Update()
    {
        Targetable target = targeter.GetTarget();
        if (target == null)
        {
            lastAttackTime = Time.time; 
            return; 
        }

        Health enemyHealth = target.GetComponent<Health>();
        if (enemyHealth == null) { return; } //do nothing

        if (!CheckInRange(target)) { return; } //still chasing by movement script

        Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);

        if (Quaternion.Angle(targetRotation, transform.rotation) > 30)
        {
            transform.rotation = Quaternion.RotateTowards(
                transform.rotation,
                targetRotation,
                rotationSpeed * Time.deltaTime);
            return;
        }

        if (Time.time > (1 / attackRate) + lastAttackTime)
        {
            //attack animation
            AnimationAttack?.Invoke();

            //deal damage
            enemyHealth.DealDamage(attackDamage);

            lastAttackTime = Time.time;
        }
    }

    [Server]
    private bool CheckInRange(Targetable target)
    {
        if (target == null) return false;

        return ((targeter.GetTarget().transform.position - transform.position).sqrMagnitude
            <= attackRange * attackRange);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    private void ServerHandleDie()
    {
        enabled = false;
    }
}
