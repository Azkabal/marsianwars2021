using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitCommandGiver : MonoBehaviour
{
    [SerializeField] LayerMask commandLayerMask = new LayerMask();

    [SerializeField] private UnitSelectionHandler unitSelectionHandler = null;

    private Camera mainCamera = null;
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;

        GameOverHandler.ClientOnGameOver += ClientHandleGameOver;
    }

    private void OnDestroy()
    {
        GameOverHandler.ClientOnGameOver -= ClientHandleGameOver;
    }

    // Update is called once per frame
    void Update()
    {
        if (Mouse.current.rightButton.wasPressedThisFrame && !Keyboard.current.shiftKey.isPressed)
        {
            ProccessOnlyRightMouseClick();
            return;
        }
        else if (Mouse.current.rightButton.wasPressedThisFrame && Keyboard.current.shiftKey.isPressed)
        {
            ProccessShiftAndRightMouseClick();
            return;
        }
        else if (Mouse.current.leftButton.wasPressedThisFrame && Keyboard.current.shiftKey.isPressed)
        {
            ProccessCtrlAndLeftMouseClick();
            return;
        }
        else if (Keyboard.current.aKey.isPressed && Keyboard.current.leftCtrlKey.isPressed)
        {
            ProccessCtrlAndAKeyPressed();
            return;
        }
    }

    private void ProccessCtrlAndAKeyPressed()
    {
        unitSelectionHandler.SelectAllUnits();
    }

    private void ProccessCtrlAndLeftMouseClick()
    {
        Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

        if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, commandLayerMask)) { return; }

        if (hit.collider.TryGetComponent<Unit>(out Unit unit))
        {
            if (unit.hasAuthority)
            {
                unitSelectionHandler.SortUnitsByType(unit.GetUnitType());
            }

            return;
        }
    }

    private void ProccessOnlyRightMouseClick()
    {
            Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, commandLayerMask)) { return; }

            if (hit.collider.TryGetComponent<Targetable>(out Targetable target))
            {
                if (target.hasAuthority)
                {
                    TryMove(hit.point);
                    return;
                }

                TryTarget(target);
                return;
            }

            TryMove(hit.point);
    }
    private void ProccessShiftAndRightMouseClick()
    {
        Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());

        if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, commandLayerMask)) { return; }

        TryMoveAndAttack(hit.point);
    }

    private void TryMove(Vector3 point)
    {
        foreach (Unit unit in unitSelectionHandler.SelectedUnits)
        {
            unit.GetUnitMovement().CmdMove(point);
        }
    }
    private void TryMoveAndAttack(Vector3 point)
    {
        foreach (Unit unit in unitSelectionHandler.SelectedUnits)
        {
            unit.GetUnitMovement().CmdMoveAndAttack(point);
        }
    }
    private void TryTarget(Targetable target)
    {
        if (target == null) return;

        foreach (Unit unit in unitSelectionHandler.SelectedUnits)
        {
            unit.GetTargeter().CmdSetTarget(target.gameObject);
        }
    }

    private void ClientHandleGameOver(string playerId)
    {
        enabled = false;
    }
}
