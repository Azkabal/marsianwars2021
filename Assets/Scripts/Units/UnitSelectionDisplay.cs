using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitSelectionDisplay : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] private TMP_Text unitNumberText = null;
    [SerializeField] private Image unitImage = null;

    [Header("Settings")]
    [SerializeField] private Unit unit_prefab = null;

    private UnitSelectionHandler unitSelectionHandler = null;

    private void Start()
    {
        unitSelectionHandler = FindObjectOfType<UnitSelectionHandler>();

        unitSelectionHandler.UnitsSelected += ClientHandleUnitsSelected;
        unitSelectionHandler.UnitsDeselected += ClientHandleUnitsDeselected;

        unitNumberText.text = "0";
        unitImage.sprite = unit_prefab.GetUnitIcon();

        unitImage.gameObject.SetActive(false);
        unitNumberText.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        unitSelectionHandler.UnitsSelected -= ClientHandleUnitsSelected;
        unitSelectionHandler.UnitsDeselected -= ClientHandleUnitsDeselected;
    }

    private void ClientHandleUnitsSelected()
    {

        List<Unit> units = unitSelectionHandler.SelectedUnits;

        int count = 0;
        foreach(Unit unit in units)
        {
            if(unit.GetUnitType() == unit_prefab.GetUnitType())
            {
                count++;
            }
        }

        unitNumberText.text = count.ToString();

        if (count != 0)
        {
            unitImage.gameObject.SetActive(true);
            unitNumberText.gameObject.SetActive(true);
        }
    }
    private void ClientHandleUnitsDeselected()
    {
        unitImage.gameObject.SetActive(false);
        unitNumberText.gameObject.SetActive(false);
    }
}
