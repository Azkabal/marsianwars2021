using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class UnitMovement : NetworkBehaviour
{
    //[SerializeField] Transform[] aligningObjects = null;
    //[SerializeField] LayerMask alignFloorMask = new LayerMask();
    public enum UnitStates
    {
        Idle,
        Move,
        MoveAndAttack,
        Attack,
        Wait
    }

    public UnitStates currentState = UnitStates.Wait;
    private bool moveAndAttackState = false;
    private Vector3 moveAndAttackDestination;

    [Header("References")]
    [SerializeField]
    private NavMeshAgent agent = null;
    [SerializeField]
    private Targeter targeter = null;
    [SerializeField]
    private LineOfSight lineOfSight = null;
    [SerializeField]
    private ParticleSystem trailLeft = null;
    [SerializeField]
    private ParticleSystem trailRight = null;

    [Header("Settings")]
    [SerializeField]
    public float chaseRange = 10f;
    [SerializeField]
    public float waitBeforeIdle = 0.5f;


    private ParticleSystem.EmissionModule emissionL;
    private ParticleSystem.EmissionModule emissionR;
    private bool renderTrails = false;

    public override void OnStartServer()
    {
        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;

        if (trailLeft == null) return;
        emissionL = trailLeft.emission;
        emissionL.enabled = false;

        if (trailRight == null) return;
        emissionR = trailRight.emission;
        emissionL.enabled = false;

        renderTrails = true;
    }

    public override void OnStopServer()
    {
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    }

    [Server]
    private void ServerHandleGameOver()
    {
        agent.ResetPath();
    }

    private float waitBeforeIdleTimer = 0f;

    [ServerCallback]
    private void Update()
    {
        switch (currentState)
        {
            case UnitStates.Idle:
                StateIdle();
                break;
            case UnitStates.Move:
                StateMove();
                break;
            case UnitStates.MoveAndAttack:
                StateMoveAndAttack();
                break;
            case UnitStates.Attack:
                StateAttack();
                break;
            case UnitStates.Wait:
                StateWait();
                break;
        }

        RenderTrails();
        ChangeRotation();

        return;
    }

    private void ChangeRotation()
    {
        //TODO:Implement later
    }

    private void RenderTrails()
    {
        if(renderTrails)
        {
            if (agent.velocity.magnitude > Mathf.Epsilon)
            {
                emissionL.enabled = true;
                emissionR.enabled = true;
            }
            else
            {
                emissionL.enabled = false;
                emissionR.enabled = false;
            }
        }
    }

    private bool LookForTarget()
    {
        Targetable possibleTarget = lineOfSight.GetTargetInSight();

        if(possibleTarget != null)
        {
            ServerTrySetTarget(possibleTarget.gameObject);
            return true;
        }

        return false;
    }

    private void StateIdle()
    {
        if (moveAndAttackState)
        {
            currentState = UnitStates.MoveAndAttack;
            return;
        }

        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = UnitStates.Attack;
            return;
        }

        if(agent.hasPath)
        {
            currentState = UnitStates.Move;
            return;
        }

        LookForTarget();
    }
    private void StateAttack()
    {
        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            //optimised stage - better than Vector3.Distance
            if ((target.transform.position - transform.position).sqrMagnitude > chaseRange * chaseRange)
            {
                //chase
                agent.SetDestination(target.transform.position);
            }
            else if (agent.hasPath)
            {
                //stop chasing
                agent.ResetPath();
            }

            return;
        }
        else
        {
            if(moveAndAttackState)
            {
                currentState = UnitStates.MoveAndAttack;
                return;
            }

            if(agent.hasPath)
            {
                currentState = UnitStates.Move;
                return;
            }
            else
            {
                currentState = UnitStates.Wait;
                return;
            }
        }
    }
    private void StateMove()
    {
        if (moveAndAttackState)
        {
            currentState = UnitStates.MoveAndAttack;
            return;
        }

        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = UnitStates.Attack;
            return;
        }


        if (!agent.hasPath)
        {
            currentState = UnitStates.Wait;
            return;
        }


        if (agent.remainingDistance > agent.stoppingDistance)
        {
            return;
        }

        agent.ResetPath();
    }
    private void StateMoveAndAttack()
    {
        if(!moveAndAttackState)
        {
            currentState = UnitStates.Wait;
            return;
        }

        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = UnitStates.Attack;
            return;
        }
        else
        {
            if(LookForTarget())
            {
                return;
            }
            else
            {
                agent.SetDestination(moveAndAttackDestination);
            }
        }


        if (!agent.hasPath)
        {
            currentState = UnitStates.Wait;
            return;
        }


        if (agent.remainingDistance > agent.stoppingDistance)
        {
            return;
        }

        agent.ResetPath();
        moveAndAttackState = false;
    }
    private void StateWait()
    {
        if (moveAndAttackState)
        {
            currentState = UnitStates.MoveAndAttack;
            return;
        }

        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            currentState = UnitStates.Attack;
            return;
        }

        if (agent.hasPath)
        {
            currentState = UnitStates.Move;
            return;
        }

        waitBeforeIdleTimer += Time.deltaTime;
        if(waitBeforeIdleTimer > waitBeforeIdle)
        {
            waitBeforeIdleTimer = Mathf.Epsilon;
            currentState = UnitStates.Idle;
            return;
        }
    }


    #region Server

    [Server]
    public void ServerMove(Vector3 position)
    {
        targeter.ServerClearTarget();

        //agent.ResetPath();

        /*
        if (!NavMesh.SamplePosition(position, out NavMeshHit hit, 1f, NavMesh.AllAreas))
        {
            return;
        }
        */

        agent.SetDestination(position);
    }

    [Command]
    public void CmdMove(Vector3 position)
    {
        moveAndAttackState = false;
        ServerMove(position);
    }
    [Command]
    public void CmdMoveAndAttack(Vector3 position)
    {
        moveAndAttackState = true;
        moveAndAttackDestination = position;
        ServerMove(position);
    }

    [Server]
    private void ServerTrySetTarget(GameObject possibleTarget)
    {
        if (possibleTarget == null) return;

        targeter.ServerSetTarget(possibleTarget);
    }
    #endregion

    #region Client

    #endregion
}
