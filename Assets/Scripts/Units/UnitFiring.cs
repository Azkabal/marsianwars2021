using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitFiring : NetworkBehaviour
{
    [Header("References")]
    [SyncVar]
    [Tooltip("Is this object has a separated rotateable part?")]
    [SerializeField] private Transform rotatableObject = null;
    [SerializeField] private Targeter targeter;
    [SerializeField] private GameObject projectile_prefab = null;
    [SerializeField] private Transform projectileSpawnPoint = null;
    [SerializeField] private Building myBuilding = null;

    [Header("Settings")]
    [SerializeField] private bool isArtillery = false;
    [Tooltip("minimum range is valid only if Artillary is ON?")]
    [SerializeField] private float minRange = 1f;
    [SerializeField] private float fireRange = 5f;
    [SerializeField] private float fireRate = 1f;
    [SerializeField] private float rotationSpeed = 20f;
    [SerializeField] private bool isBuilding = false;

    private float lastFireTime = 0f;

    private RTSPlayer player;

    private void Start()
    {
        if(rotatableObject == null)
        {
            rotatableObject = this.transform;
        }
    }

    public override void OnStartServer()
    {
        player = connectionToClient.identity.GetComponent<RTSPlayer>();
    }

    public float GetFireRange()
    {
        return fireRange;
    }

    [ServerCallback]
    private void Update()
    {
        if(isBuilding)
        {
            if (!myBuilding.IsFinished()) return;
        }

        Targetable target = targeter.GetTarget();
        if (target == null) { return; }

        if (!CheckIfEnemyInRange()) { return; }

        Quaternion targetRotation =
            Quaternion.LookRotation(target.transform.position - transform.position);

        //the targetRotation is a Vector pointing to target!
        rotatableObject.rotation = Quaternion.RotateTowards(
            rotatableObject.rotation,
            targetRotation,
            rotationSpeed * Time.deltaTime);

        if (Quaternion.Angle(targetRotation, rotatableObject.rotation) > 30) { return; }

        float modifiedFireRate = (100f + player.GetUpdateFireRate()) * fireRate / 100f;

        if (Time.time > (1 / modifiedFireRate) + lastFireTime)
        {
            Fire(target);

            lastFireTime = Time.time;
        }
    }

    private void Fire(Targetable target)
    {
        if(!isArtillery)
        {
            Quaternion projectileRotation = Quaternion.LookRotation(
                target.GetAimAtPoint().position - projectileSpawnPoint.position);
            //Get a Vector from projectile start point to projectile end point!

            GameObject projectileInstance = Instantiate(
                projectile_prefab,
                projectileSpawnPoint.position,
                projectileRotation);

            NetworkServer.Spawn(projectileInstance, connectionToClient);
        }
        else
        {
            Vector3 targetPos = target.GetAimAtPoint().position;
            Vector3 spawnPos = projectileSpawnPoint.position;

            Vector3 modTargetPos = new Vector3(targetPos.x, spawnPos.y, targetPos.z);

            Quaternion projectileRotation = Quaternion.LookRotation(
                modTargetPos - spawnPos);

            GameObject projectileInstance = Instantiate(
                projectile_prefab,
                projectileSpawnPoint.position,
                projectileRotation);

            float angleValue = Mathf.Abs(projectileInstance.GetComponent<UnitProjectile>().GetAngle());

            float sinVal = Mathf.Sin(angleValue / 360f * 2f * Mathf.PI);
            float cosVal = Mathf.Cos(angleValue / 360f * 2f * Mathf.PI);


            float extDistance = Vector3.Distance(targetPos, spawnPos);
            float gravityV = Mathf.Abs(Physics.gravity.y);

            float applyLaunchForce = Mathf.Sqrt(gravityV * extDistance / (2f * sinVal * cosVal));

            NetworkServer.Spawn(projectileInstance, connectionToClient);

            float forwardM = applyLaunchForce * cosVal;
            float upM = applyLaunchForce * sinVal;
            //sync with clients (clientRPC)
            projectileInstance.GetComponent<UnitProjectile>().ApplyForceToRigidbody(forwardM, upM);
            projectileInstance.GetComponent<UnitProjectile>().RpcSyncRigidbody(forwardM, upM);

        }
    }

    [Server]
    private bool CheckIfEnemyInRange()
    {
        if(isArtillery)
        {
            return ((targeter.GetTarget().transform.position - transform.position).sqrMagnitude
                <= fireRange * fireRange) && ((targeter.GetTarget().transform.position - transform.position).sqrMagnitude >= minRange * minRange);
        }
        else
        {
            return ((targeter.GetTarget().transform.position - transform.position).sqrMagnitude
                <= fireRange * fireRange);
        }
    }
}
