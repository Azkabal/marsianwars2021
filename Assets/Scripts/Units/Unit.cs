using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Events;
using System;
using UnityEngine.AI;

public class Unit : NetworkBehaviour
{
    [SerializeField] private int resourceCost = 10;
    [SerializeField] private int energyCost = 1;
    [SerializeField] private int unitType = -1;
    [SerializeField] private Sprite unitIcon = null;

    [SerializeField] private Health health = null;
    [SerializeField] private UnitMovement unitMovement = null;
    [SerializeField] private Targeter targeter = null;
    [SerializeField] private UnityEvent onSelected = null;
    [SerializeField] private UnityEvent onDeselected = null;

    public static event Action<Unit> ServerOnUnitSpawned;
    public static event Action<Unit> ServerOnUnitDespawned;

    public static event Action<Unit> AuthorityOnUnitSpawned;
    public static event Action<Unit> AuthorityOnUnitDespawned;

    private bool isDead = false;

    public int GetResourceCost()
    {
        return resourceCost;
    }
    public int GetEnergyCost()
    {
        return energyCost;
    }
    public int GetUnitType()
    {
        return unitType;
    }
    public Sprite GetUnitIcon()
    {
        return unitIcon;
    }

    public UnitMovement GetUnitMovement()
    {
        return unitMovement;
    }
    public Targeter GetTargeter()
    {
        return targeter;
    }

    #region Server

    public override void OnStartServer()
    {
        health.ServerOnDie += ServerHandleDie;
        ServerOnUnitSpawned?.Invoke(this);
    }

    public override void OnStopServer()
    {
        health.ServerOnDie -= ServerHandleDie;
    }

    private void Update()
    {
        if(isDead)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.2f * Time.deltaTime, transform.position.z);
        }
    }

    [Server]
    private void ServerHandleDie()
    {
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<UnitMovement>().enabled = false;
        GetComponent<UnitFiring>().enabled = false;
        GetComponent<Health>().enabled = false;
        GetComponent<HealthDisplay>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        GetComponentInChildren<LineOfSight>().enabled = false;
        Destroy(GetComponent<Targetable>());
        ServerOnUnitDespawned?.Invoke(this);
        isDead = true;
        RpcUnitDespawned();
        Invoke("DestroyMe", 15f); 
    }

    private void DestroyMe()
    {
        NetworkServer.Destroy(gameObject);
    }



    #endregion

    #region Client

    public override void OnStartAuthority()
    {
        // we are not server and having authority
        AuthorityOnUnitSpawned?.Invoke(this);
    }

    [TargetRpc]
    private void RpcUnitDespawned()
    {
        AuthorityOnUnitDespawned?.Invoke(this);
    }

    public override void OnStopClient()
    {
        if (!hasAuthority)
        {
            return;
        }
        // we are not server and having authority
    }

    [Client]
    public void Select()
    {
        if (!hasAuthority) return;

        onSelected?.Invoke();
    }
    [Client]
    public void Deselect()
    {
        if (!hasAuthority) return;

        onDeselected?.Invoke();
    }
    #endregion
}
