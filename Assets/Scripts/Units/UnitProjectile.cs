using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitProjectile : NetworkBehaviour
{
    [Header("Reference")]
    [SerializeField] private Rigidbody rb;
    [SerializeField] private GameObject VFXprefab;

    [Header("Settings")]
    [SerializeField] private int damageToDeal = 20;
    [SerializeField] private float destroyAfterSeconds = 10f;
    [SerializeField] private float launchForce = 10f;
    [SerializeField] private bool isArtillery = false;
    [Tooltip("angle is valid only if artillery ON")]
    [SerializeField] private float angle = -45;
    [SerializeField] private float artilleryRadius = 5f;
    [SerializeField] private LayerMask artilleryMask = new LayerMask();


    public float GetAngle()
    {
        return angle;
    }

    private void Start()
    {
        if (isArtillery)
        {
            transform.Rotate(angle, 0, 0);
        }
        else
        {
            rb.velocity = transform.forward * launchForce;
        }

    }

    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), destroyAfterSeconds);
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other == null) return;

        if(other.TryGetComponent<NetworkIdentity>(out NetworkIdentity id))
        {
            //the projectile has hitted our unit?
            if(id.connectionToClient == connectionToClient) { return; }
        }

        if (!isArtillery)
        {
            //the projectile has hitted enemy unit?
            if (other.TryGetComponent<Health>(out Health health))
            {
                health.DealDamage(damageToDeal);
            }
        }
        else
        {
            Collider[] colliders =
                Physics.OverlapSphere(
                    transform.position,
                    artilleryRadius,
                    artilleryMask
                    );


            if (other.TryGetComponent<Health>(out Health health))
            {
                health.DealDamage(damageToDeal);
            }

            foreach (Collider collider in colliders)
            {
                if (!collider.TryGetComponent<Health>(out Health colliderHealth)) continue;

                if (colliderHealth.connectionToClient == connectionToClient) continue;

                if(health != null)
                {
                    if (colliderHealth == health) continue; // to prevent double damage
                }

                colliderHealth.DealDamage(damageToDeal);
            }
        }

        //we have hitted something - destroy projectile
        DestroySelf();
    }

    [Server]
    private void DestroySelf()
    {
        if (VFXprefab != null)
        {
            GameObject vfxObject = Instantiate(VFXprefab, this.transform.position, Quaternion.identity);

            NetworkServer.Spawn(vfxObject);
        }

        NetworkServer.Destroy(gameObject);
    }

    [Server]
    public void ApplyForceToRigidbody(float forwardM, float upM)
    {
        rb.velocity = (transform.forward * forwardM + transform.up * upM);
    }

    [ClientRpc]
    public void RpcSyncRigidbody(float forwardM, float upM)
    {
        if (isServer) return;

        rb.velocity = (transform.forward * forwardM + transform.up * upM);
    }
}
